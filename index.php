<?php

require_once('db/Dbconnection.php');
header('Access-Control-Allow-Origin: *');

class UrbanEyeApiClass
{
    // DB
    public $conn;
    protected $connection;
    protected $query;
    protected $show_errors = TRUE;
    protected $query_closed = TRUE;
    public $query_count = 0;
    public $SITE_URL = 'http://192.168.9.201/UrbanEyeApi/';
    // public $SITE_URL = 'http://staging.asanbuy.pk/';


    //function __construct($db)
    function __construct($db)
    {
        $this->conn = $db;
        $this->apiResponse = array('Response' => 'Invalid API Request');
    }
    function index()
    {
        $req = $_GET['request'];
        switch ($req) {
            case "get-details-lenses":
                $this->apiResponse = $this->getDetailsLenses();
                break;
            case "get-details-lenses-history":
                $this->apiResponse = $this->getDetailsLenses2();
                break;
            case "search-customer-details":
                $this->apiResponse = $this->searchCustomerDetails();
                break;
            case "search-customer-details-for-add":
                $this->apiResponse = $this->searchCustomerDetails2();
                break;
            case "search-customer":
                $this->apiResponse = $this->searchCustomerNameID();
                break;
            case "add-data-tbl1":
                $this->apiResponse = $this->addDataTbl1();
                break;
            default:
                // $this->apiResponse = $this->getHome();
                break;
        }

        echo json_encode($this->apiResponse);
        //exit;
    }


    public function query($query)
    {
        if (!$this->query_closed) {
            $this->query->close();
        }
        if ($this->query = $this->conn->prepare($query)) {
            if (func_num_args() > 1) {
                $x = func_get_args();
                $args = array_slice($x, 1);
                $types = '';
                $args_ref = array();
                foreach ($args as $k => &$arg) {
                    if (is_array($args[$k])) {
                        foreach ($args[$k] as $j => &$a) {
                            $types .= $this->_gettype($args[$k][$j]);
                            $args_ref[] = &$a;
                        }
                    } else {
                        $types .= $this->_gettype($args[$k]);
                        $args_ref[] = &$arg;
                    }
                }
                array_unshift($args_ref, $types);
                call_user_func_array(array($this->query, 'bind_param'), $args_ref);
            }
            $this->query->execute();
            if ($this->query->errno) {
                $this->error('Unable to process MySQL query (check your params) - ' . $this->query->error);
            }
            $this->query_closed = FALSE;
            $this->query_count++;
        } else {
            $this->error('Unable to prepare MySQL statement (check your syntax) - ' . $this->conn->error);
        }
        return $this;
    }

    public function fetchAll($callback = null)
    {
        $params = array();
        $row = array();
        $meta = $this->query->result_metadata();
        while ($field = $meta->fetch_field()) {
            $params[] = &$row[$field->name];
        }
        call_user_func_array(array($this->query, 'bind_result'), $params);
        $result = array();
        while ($this->query->fetch()) {
            $r = array();
            foreach ($row as $key => $val) {
                $r[$key] = $val;
            }
            if ($callback != null && is_callable($callback)) {
                $value = call_user_func($callback, $r);
                if ($value == 'break') break;
            } else {
                $result[] = $r;
            }
        }
        $this->query->close();
        $this->query_closed = TRUE;
        return $result;
    }

    public function fetchArray()
    {
        $params = array();
        $row = array();
        $meta = $this->query->result_metadata();
        while ($field = $meta->fetch_field()) {
            $params[] = &$row[$field->name];
        }
        call_user_func_array(array($this->query, 'bind_result'), $params);
        $result = array();
        while ($this->query->fetch()) {
            foreach ($row as $key => $val) {
                $result[$key] = $val;
            }
        }
        $this->query->close();
        $this->query_closed = TRUE;
        return $result;
    }

    public function close()
    {
        return $this->conn->close();
    }

    public function numRows()
    {
        $this->query->store_result();
        return $this->query->num_rows;
    }

    public function affectedRows()
    {
        return $this->query->affected_rows;
    }

    public function lastInsertID()
    {
        return $this->conn->insert_id;
    }

    public function error($error)
    {
        if ($this->show_errors) {
            exit($error);
        }
    }

    private function _gettype($var)
    {
        if (is_string($var)) return 's';
        if (is_float($var)) return 'd';
        if (is_int($var)) return 'i';
        return 'b';
    }

    public function searchCustomerDetails2()
    {
        $customer_id = $_GET['customer_id'];
        $q1 = "SELECT * FROM `tblcustomers` WHERE `CustomerID` = '$customer_id'";
        $q2 = "SELECT *, date(OrderDate) as order_date_new, date(ActualDeliveryDate) as actual_delivery_date_new from `tblcl` where `customerID` = '$customer_id' order by OrderID desc limit 1";
        $q3 = "SELECT *, date(OrderDate) as order_date_new, date(ActualDeliveryDate) as actual_delivery_date_new from `tblspect` where `customerid` = '$customer_id' order by OrderID desc limit 1";
        $data['Response']['Info'] = $this->query($q1)->fetchAll();
        $data['Response']['ContactLens'] = $this->query($q2)->fetchAll();
        $data['Response']['SpectacleLens'] = $this->query($q3)->fetchAll();
        return $data;
    }

    public function searchCustomerDetails()
    {
        $customer_id = $_GET['customer_id'];
        $q1 = "SELECT * FROM `tblcustomers` WHERE `CustomerID` = '$customer_id'";
        // $q2 = "SELECT * from `tblcl` where `customerID` = '$customer_id' order by OrderID desc";
        // $q3 = "SELECT * from `tblspect` where `customerid` = '$customer_id' order by OrderID desc";
        $data['Response']['Info'] = $this->query($q1)->fetchAll();
        // $data['Response']['ContactLens'] = $this->query($q2)->fetchAll();
        // $data['Response']['SpectacleLens'] = $this->query($q3)->fetchAll();
        return $data;
    }

    public function getDetailsLenses()
    {
        $customer_id = $_GET['customer_id'];
        $type = $_GET['type'];

        // 1 Contact Lense
        if ($type === 1 || $type === '1') {
            $q = "SELECT *, DATE_FORMAT(`OrderDate`, '%d %M %Y') AS new_order_date from `tblcl` where `customerID` = '$customer_id' order by OrderID desc limit 1";
        } elseif ($type === 0 || $type === '0') {
            $q = "SELECT *, DATE_FORMAT(`OrderDate`, '%d %M %Y') AS new_order_date from `tblspect` where `customerid` = '$customer_id' order by OrderID desc limit 1";
        }
        $data['Response']['Data'] = $this->query($q)->fetchAll();
        $data['Response']['Type'] = $type;
        return $data;
    }

    public function getDetailsLenses2()
    {
        $customer_id = $_GET['customer_id'];
        $type = $_GET['type'];

        // 1 Contact Lense
        if ($type === 1 || $type === '1') {
            $q = "SELECT *, DATE_FORMAT(`OrderDate`, '%d %M %Y') AS new_order_date from `tblcl` where `customerID` = '$customer_id' order by OrderID desc";
        } elseif ($type === 0 || $type === '0') {
            $q = "SELECT *, DATE_FORMAT(`OrderDate`, '%d %M %Y') AS new_order_date from `tblspect` where `customerid` = '$customer_id' order by OrderID desc";
        }
        $data['Response']['Data'] = $this->query($q)->fetchAll();
        $data['Response']['Type'] = $type;
        return $data;
    }

    public function searchCustomerNameID()
    {
        $customer_name = $_GET['customer_name'];
        $colName = $_GET['colName'];
        $q = "SELECT CustomerID as id, Title as prefix, CustomerName as customer_name FROM tblcustomers where $colName like '%$customer_name%'  limit 25";
        $result = $this->query($q)->fetchAll();
        $array = [];
        foreach ($result as $key => $val) {
            $val['img_url'] = $this->SITE_URL . 'assets/user-icon.webp';
            array_push($array, $val);
        }
        $data['Response']['Result'] =  $array;

        return $data;
    }

    public function addDataTbl1()
    {

        // print_r($_GET);

        // Customer Details
        $CustomerID = $_GET['CustomerID'];
        $CustomerName = $_GET['CustomerName'];
        $Phone = $_GET['Phone'];
        $Mobile = $_GET['Mobile'];
        $Address = $_GET['Address'];
        $Title = $_GET['Title'];
        $Photo = $_GET['Photo'];
        $DoB = $_GET['DoB'];

        // Contact Lense
        // $clOrderID = $_GET['clOrderID'];
        $clRightEyePwr = $_GET['clRightEyePwr'];
        $clLeftEyePwr = $_GET['clLeftEyePwr'];

        $clRightEyeBC = $_GET['clRightEyeBC'];
        $clLeftEyeBC = $_GET['clLeftEyeBC'];

        $clRightEyeDIA = $_GET['clRightEyeDIA'];
        $clLeftEyeDIA = $_GET['clLeftEyeDIA'];

        $clRightEyeAxis = $_GET['clRightEyeAxis'];
        $clLeftEyeAxis = $_GET['clLeftEyeAxis'];

        $clRightEyeSph = $_GET['clRightEyeSph'];
        $clLeftEyeSph = $_GET['clLeftEyeSph'];

        $clRightEyeCyl = $_GET['clRightEyeCyl'];
        $clLeftEyeCyl = $_GET['clLeftEyeCyl'];


        $clMake = $_GET['clMake'];
        $clColor = $_GET['clColor'];
        $clOrderDate = $_GET['clOrderDate'];
        $clDeliveryDate = $_GET['clDeliveryDate'];
        $clAdvancePayment = $_GET['clAdvancePayment'];
        $clTotalCharges = $_GET['clTotalCharges'];
        $clReceivable = $_GET['clReceivable'];
        $clReferenceNo = $_GET['clReferenceNo'];
        $clRemarks = $_GET['clRemarks'];
        $clDoctorName = $_GET['clDoctorName'];
        $clDelivered = $_GET['clDelivered'];
        $clActualDeliveryDate = $_GET['clActualDeliveryDate'];
        $clBalanceReceivingDate = $_GET['clBalanceReceivingDate'];

        // Spectacles Lens
        // $slcustomerid = $_GET['slcustomerid'];
        $slRightEyeCyl = $_GET['slRightEyeCyl'];
        $slLeftEyeCyl = $_GET['slLeftEyeCyl'];

        $slRightEyeSph = $_GET['slRightEyeSph'];
        $slLeftEyeSph = $_GET['slLeftEyeSph'];

        $slRightEyeAxis = $_GET['slRightEyeAxis'];
        $slLeftEyeAxis = $_GET['slLeftEyeAxis'];

        $slRightEyeAddition = $_GET['slRightEyeAddition'];
        $slLeftEyeAddition = $_GET['slLeftEyeAddition'];

        $slRightEyeMid = $_GET['slRightEyeMid'];
        $slLeftEyeMid = $_GET['slLeftEyeMid'];

        $slRightEyePD = $_GET['slRightEyePD'];
        $slLeftEyePD = $_GET['slLeftEyePD'];

        $slLenses = $_GET['slLenses'];
        $slFrame = $_GET['slFrame'];
        $slDeliveryDate = $_GET['slDeliveryDate'];
        $slOrderDate = $_GET['slOrderDate'];
        $slTotalCharges = $_GET['slTotalCharges'];
        $slAdvancePayment = $_GET['slAdvancePayment'];
        $slReceivable = $_GET['slReceivable'];
        $slReferenceNo = $_GET['slReferenceNo'];
        $slRemarks = $_GET['slRemarks'];
        $slDoctorName = $_GET['slDoctorName'];
        
        $slDelivered = $_GET['slDelivered'];
        $slActualDeliveryDate = $_GET['slActualDeliveryDate'];
        $slBalanceReceivingDate = $_GET['slBalanceReceivingDate'];

        // Check User Exist 
        $user_exist_q = "SELECT CustomerID from tblcustomers where `CustomerID` = '$CustomerID'";
        $check_user_exist = $this->query($user_exist_q)->fetchAll();
        if (!empty($check_user_exist)) {
        } else {
            // Insert Customer 
            $q = "INSERT INTO `tblcustomers` (`CustomerName`, `Phone`, `Mobile`, `Address`, `DoB`, `Title`, `Photo` ) VALUES ( '$CustomerName', '$Phone', '$Mobile', '$Address', '$DoB', '$Title', '$Photo' )";
            if (mysqli_query($this->conn, $q)) {
                $CustomerID =  $this->lastInsertID();
                echo $CustomerID;
            }
        }

        //$insert_cl_q =  "INSERT INTO `tblcl` ( `RightEyePwr`, `customerID`, `RightEyeBC`, `RightEyeDIA`, `LeftEyePwr`, `LeftEyeBC`, `LeftEyeDIA`, `Make`, `Color`, `OrderDate`, `DeliveryDate`, `AdvancePayment`, `TotalCharges`, `Receivable`, `ReferenceNo`, `Remarks`, `DoctorName`, `Delivered`, `ActualDeliveryDate`, `BalanceReceivingDate` ) VALUES ( '$clRightEyePwr', '$CustomerID', '$clRightEyeBC', '$clRightEyeDIA', '$clLeftEyePwr', '$clLeftEyeBC', '$clLeftEyeDIA', '$clMake', '$clColor', '$clOrderDate', '$clDeliveryDate', '$clAdvancePayment', '$clTotalCharges', '$clReceivable', '$clReferenceNo', '$clRemarks', '$clDoctorName', '$clDelivered', '$clActualDeliveryDate', '$clBalanceReceivingDate' );";
       $insert_cl_q = "INSERT INTO `tblcl` ( `customerID`, `RightEyePwr`, `LeftEyePwr`, `RightEyeBC`, `LeftEyeBC`, `RightEyeDIA`, `LeftEyeDIA`, `RightEyeAxis`, `LeftEyeAxis`, `RightEyeSph`, `LeftEyeSph`, `RightEyeCyl`, `LeftEyeCyl`, `Make`, `Color`, `OrderDate`, `DeliveryDate`, `AdvancePayment`, `TotalCharges`, `Receivable`, `ReferenceNo`, `Remarks`, `DoctorName`, `Delivered`, `ActualDeliveryDate`, `BalanceReceivingDate` ) VALUES ( '$CustomerID', '$clRightEyePwr', '$clLeftEyePwr', '$clRightEyeBC', '$clLeftEyeBC', '$clRightEyeDIA', '$clLeftEyeDIA', '$clRightEyeAxis', '$clLeftEyeAxis', '$clRightEyeSph', '$clLeftEyeSph', '$clRightEyeCyl', '$clLeftEyeCyl', '$clMake', '$clColor', '$clOrderDate', '$clDeliveryDate', '$clAdvancePayment', '$clTotalCharges', '$clReceivable', '$clReferenceNo', '$clRemarks', '$clDoctorName', '$clDelivered', '$clActualDeliveryDate', '$clBalanceReceivingDate' );";
       if (mysqli_query($this->conn, $insert_cl_q)) {
            echo 'CL Done';
       }

       // $insert_sl_q = "INSERT INTO `tblspect` ( `customerid`, `RightEyeSph`, `RightEyeCyl`, `RightEyeAddition`, `RightEyeAxis`, `LeftEyeSph`, `LeftEyeCyl`, `LeftEyeAxis`, `LeftEyeAddition`, `Lenses`, `Frame`, `DeliveryDate`, `OrderDate`, `TotalCharges`, `AdvancePayment`, `Receivable`, `ReferenceNo`, `Remarks`, `DoctorName`, `RightEyePD`, `LeftEyePD`, `Delivered`, `ActualDeliveryDate`, `BalanceReceivingDate` ) VALUES ( '$CustomerID', '$slRightEyeSph', '$slRightEyeCyl', '$slRightEyeAddition', '$slRightEyeAxis', '$slLeftEyeSph', '$slLeftEyeCyl', '$slLeftEyeAxis', '$slLeftEyeAddition', '$slLenses', '$slFrame', '$slDeliveryDate', '$slOrderDate', '$slTotalCharges', '$slAdvancePayment', '$slReceivable', '$slReferenceNo', '$slRemarks', '$slDoctorName', '$slRightEyePD', '$slLeftEyePD', '$slDelivered', '$slActualDeliveryDate', '$slBalanceReceivingDate' );";
       $insert_sl_q = "INSERT INTO `tblspect` ( `customerid`, `RightEyeSph`, `LeftEyeSph`, `RightEyeCyl`, `LeftEyeCyl`, `RightEyeAddition`, `LeftEyeAddition`, `RightEyeAxis`, `LeftEyeAxis`, `RightEyePD`, `LeftEyePD`, `RightEyeMid`, `LeftEyeMid`, `Lenses`, `Frame`, `DeliveryDate`, `OrderDate`, `TotalCharges`, `AdvancePayment`, `Receivable`, `ReferenceNo`, `Remarks`, `DoctorName`, `Delivered`, `ActualDeliveryDate`, `BalanceReceivingDate` ) VALUES ( '$CustomerID', '$slRightEyeSph', '$slLeftEyeSph', '$slRightEyeCyl', '$slLeftEyeCyl', '$slRightEyeAddition', '$slLeftEyeAddition', '$slRightEyeAxis', '$slLeftEyeAxis', '$slRightEyePD', '$slLeftEyePD', '$slRightEyeMid', '$slLeftEyeMid', '$slLenses', '$slFrame', '$slDeliveryDate', '$slOrderDate', '$slTotalCharges', '$slAdvancePayment', '$slReceivable', '$slReferenceNo', '$slRemarks', '$slDoctorName', '$slDelivered', '$slActualDeliveryDate', '$slBalanceReceivingDate' );";
       if (mysqli_query($this->conn, $insert_sl_q)) {
             echo 'SL Done';
       }

       return 1;
    }
}


header('Access-Control-Allow-Origin: *');

$initDB = new DbConnection();
$conn = $initDB->getdbconnect();
$urbanEye = new UrbanEyeApiClass($conn);
$urbanEye->index();
