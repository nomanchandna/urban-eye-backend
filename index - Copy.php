<?php

require './vendor/autoload.php';

use Automattic\WooCommerce\HttpClient\HttpClientException;
use Automattic\WooCommerce\Client;

class BravoApiClass
{
    public $apiResponse;
    public $woo;
    protected $assetsFolder = "https://www.bravonutrition.pk/";
    protected $site_url = 'https://www.bravonutrition.pk/';

    function __construct()
    {
        $this->apiResponse = array('Response' => 'Invalid API Request');

        $this->woo  = new Client(
            'https://www.bravonutrition.pk/',
            'ck_b61a3cc440ddfadc278411265a0a417394cf6acb',
            'cs_eb26c9eed7f6608abc656781da91a43d443595c0',
            [
                'wp_api' => true,
                'version' => 'wc/v3',
                'query_string_auth' => true
            ]
        );
    }
    function index()
    {
        $req = $_GET['request'];
        switch ($req) {
            case "home":
                $this->apiResponse = $this->home2();
                break;
            case "getAllProducts":
                $this->apiResponse = $this->getAllProducts();
                break;
            default:
                $this->apiResponse = $this->home2();
                break;
        }

        echo json_encode($this->apiResponse);
        exit;
    }

    function getAllProducts()
    {

        $data = $this->woo->get('products');
        return !empty($data) ?  $data :  $this->apiResponse;
    }

    function home2()
    {
        $items = array();
        $sliders = json_decode(json_encode($this->woo->get('products', ['order' => 'desc', 'orderby' => 'date', 'per_page' => 4])), true);
        foreach ($sliders as $slide) {
            $products['slider_image'] = $slide['images'];
            $products['id'] = $slide['id'];
            $products['name'] = $slide['name'];
            $products['slug'] = $slide['slug'];
            $products['type'] = $slide['type'];
            $products['price'] = $slide['price'];
            $products['categories'] = $slide['categories'];
            $products['related_ids'] = $slide['related_ids'];
            $items['Slider'][] = $products;
        }

        $items['Categories'] = json_decode(json_encode($this->woo->get('products/categories', ['parent' => 0, 'per_page' => 100])), true);
        //$abc['Categories'] = json_decode(json_encode($this->woo->get('products/categories', ['order' => 'desc', 'order_by'=>'id' ,'include'=> '56,55,47,35,39,54' ,'parent' => 0, 'per_page' => 100])), true);
        // foreach ($abc['Categories'] as $cat) {
        //$cat = $slide['images'];
        //  $cat2['id'] = $cat['id'];
        //   $cat2['name'] = $cat['name'];
        // $cat['price'] = $slide['price'];
        // $cat['regular_price'] = $slide['regular_price'];
        // $cat['sale_price'] = $slide['sale_price'];
        // $cat['categories'] = $slide['categories'];
        //    $items['Categories'][] = $cat2;
        // }
        // print_r($items['Categories']);
        $catData = ['per_page' => 100, 'include' => '56,55,47,35,39,54'];
        $categories = json_decode(json_encode($this->woo->get('products/categories', $catData)), true);

        $catString = "";
        foreach ($categories as $cat) {
            if ($cat['parent'] != 0) {
                $catString = $catString . $cat['id'] . ",";
            }
        }

        $items['SubCategories'] = json_decode(json_encode($this->woo->get('products/categories', array('include' => $catString, 'per_page' => 100))), true);
        $data['per_page'] = 6;
        foreach ($categories as $category) {
            $data['category'] = $category['id'];
            $records = json_decode(json_encode($this->woo->get('products', $data)), true);
            foreach ($records as $record) {
                // $products = vpa_array_flatten($record['images']);
                $products['id'] = $record['id'];
                $products['type'] = $record['type'];
                $products['price'] = $record['price'];
                $products['regular_price'] = $record['regular_price'];
                $products['sale_price'] = $record['sale_price'];
                $products['categories'] = $record['categories'];
                $items[$category['name']][] = $products;
            }
        }
        unset($data['category'], $data['description'], $data['order'], $data['orderby']);

        print_r($items);
    }

    function home()
    {
        // $category = [];
        // $data = $this->woo->get('products/categories');
        // foreach ($data as $key => $val) {
        //     echo '<p>' . $val->image. '</p>';
        // }
        // return !empty($data) ?  $data :  $this->apiResponse;
        // try {
        //     $response['Response'] = $this->woo->get('products/categories/33');
        //     foreach ($response['Response'] as $key => &$val) {
        //         echo '<h2>' . $val->id . '</h2>';
        //     }
        // } catch (HttpClientException $e) {
        //     $response['Response'] = $e->getMessage();
        // }
        // return $response;
        // $homeData = [];

        try {
            $items = array();
            $data = array();
            $data['order'] = 'desc';
            $data['orderby'] = 'date';
            $data['per_page'] = 5;
            $sliders = json_decode(json_encode($this->woo->get('products', ['order' => 'desc', 'orderby' => 'date', 'per_page' => 5])), true);
            foreach ($sliders as $slide) {
                $products = $slide['images'];
                $products['id'] = $slide['id'];
                $products['type'] = $slide['type'];
                $products['price'] = $slide['price'];
                $products['regular_price'] = $slide['regular_price'];
                $products['sale_price'] = $slide['sale_price'];
                $products['categories'] = $slide['categories'];
                $items['Slider'][] = $products;
            }
            //$items['Categories'] = json_decode(json_encode($this->woo->get('products/categories', ['parent' => 0, 'per_page' => 100])), true);
            $items['Categories'] = json_decode(json_encode($this->woo->get('products/categories', ['parent' => 0, 'per_page' => 100])), true);
            // $catData = ['per_page' => 100, 'exclude' => '44,34,20,32,15,25'];
            // $categories = json_decode(json_encode($this->woo->get('products/categories', $catData)), true);

            // $catString = "";
            // foreach ($categories as $cat) {
            //     if ($cat['parent'] != 0) {
            //         $catString = $catString . $cat['id'] . ",";
            //     }
            // }

            // $items['SubCategories'] = json_decode(json_encode($this->woo->get('products/categories', array('include' => $catString, 'per_page' => 100))), true);
            // $data['per_page'] = 3;
            // foreach ($categories as $category) {
            //     $data['category'] = $category['id'];
            //     $records = json_decode(json_encode($this->woo->get('products', $data)), true);
            //     foreach ($records as $record) {
            //         // $products = vpa_array_flatten($record['images']);
            //         $products['id'] = $record['id'];
            //         $products['type'] = $record['type'];
            //         $products['price'] = $record['price'];
            //         $products['regular_price'] = $record['regular_price'];
            //         $products['sale_price'] = $record['sale_price'];
            //         $products['categories'] = $record['categories'];
            //         $items[$category['name']][] = $products;
            //     }
            // }
            // unset($data['category'], $data['order'], $data['orderby']);

            //$fp = fopen('/var/www/vhosts/www.greene.pk/services/assets/json_file/home_content.json', 'w');	
            //$fp = fopen('/tmp/home_content.json', 'w');
            $fp = fopen('home_content.json', 'w');
            fwrite($fp, json_encode($items));
            //fwrite($fp, "Taha");
            fclose($fp);
            // echo "Taha";
            $wdata = json_encode($items);
            `sudo echo "Taha" >>/tmp/home_content`;

            $response['Response'] = $items;
        } catch (HttpClientException $e) {
            $response['Response'] = $e->getMessage();
        }
        return $response;
    }
}

$bravo = new BravoApiClass();
$bravo->index();
