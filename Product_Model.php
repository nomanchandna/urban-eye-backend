<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Product_Model extends VPA_Model {

    protected $tableName;

    public function __construct() {
        parent::__construct();
        $this->tableName = 'tbl_products';
    }

    public function register($data) {
        $query = $this->db->get_where($this->tableName, array('Email' => $data['Email']));
        if ($query->num_rows() == 0) {
            $query = $this->db->insert($this->tableName, $data);
            return true;
        }
        return false;
    }

    public function GetSlider() {
        $array = array('tbl_products.isSlider' => '1');
        //$this->db->get_where($this->tableName, $array);
        //$this->db->join('tbl_bookings', 'tbl_products.bookingId = tbl_bookings.BookingId');
        $this->db->select("tbl_subcategory.name As CatName, tbl_category.name AS MainCatName, tbl_products.Id,  tbl_products.CatId, tbl_products.subCatId, tbl_products.name As ProdName, tbl_products.desc, tbl_products.image, tbl_products.actualPrice, tbl_products.offeredPrice, tbl_products.sortOrder, tbl_products.isClickable");
        $this->db->join('tbl_subcategory', 'tbl_subcategory.id = tbl_products.subCatId');
        $this->db->join('tbl_category', 'tbl_category.id = tbl_products.CatId');
        $query = $this->db->get_where($this->tableName, $array);
        return $query->result_array();
    }

    public function GetAllCategories() {
        $array = array('tbl_subcategory.status' => '1');
        $this->db->select('*');
        //$this->db->from('tbl_subcategory');
        $this->db->order_by('order', 'ASC');
        //return $query = $this->db->get()->result_array();
        $query = $this->db->get_where('tbl_subcategory', $array);
        return $query->result_array();

        /*$sql = "SELECT DISTINCT(pro.subCatId), subc.name FROM tbl_products pro, tbl_subcategory subc WHERE pro.subCatId = subc.id";
        $query = $this->db->query($sql);
        return $query->result_array();*/

    }

    public function GetAllParentCategories() {
        $array = array('tbl_category.status' => '1');
        $this->db->select('*');
        //$this->db->from('tbl_category');
        $this->db->order_by('order', 'ASC');
        //return $query = $this->db->get()->result_array();
        $query = $this->db->get_where('tbl_category', $array);
        return $query->result_array();

        /*$sql = "SELECT DISTINCT(pro.subCatId), subc.name FROM tbl_products pro, tbl_subcategory subc WHERE pro.subCatId = subc.id";
        $query = $this->db->query($sql);
        return $query->result_array();*/

    }

    public function HomeProducts($catId, $limit = 3) {

        $array = array('tbl_products.subCatId' => $catId);
        $this->db->select("tbl_subcategory.name As CatName, tbl_category.name AS MainCatName, tbl_products.Id,  tbl_products.CatId, tbl_products.subCatId, tbl_products.name As ProdName, tbl_products.desc, tbl_products.image, tbl_products.actualPrice, tbl_products.offeredPrice, tbl_products.sortOrder");
        $this->db->join('tbl_subcategory', 'tbl_subcategory.id = tbl_products.subCatId');
        $this->db->join('tbl_category', 'tbl_category.id = tbl_products.CatId');
        $this->db->order_by('tbl_products.sortOrder', 'DESC');
        return $this->db->get_where($this->tableName, $array , $limit)->result_array();

    }

    public function GetProdById($array) {
        $this->db->select("tbl_subcategory.name As CatName, tbl_category.name AS MainCatName, tbl_products.Id,  tbl_products.CatId, tbl_products.subCatId, tbl_products.name As ProdName, tbl_products.desc, tbl_products.image, tbl_products.actualPrice, tbl_products.offeredPrice, tbl_products.sortOrder");
        $this->db->join('tbl_subcategory', 'tbl_subcategory.id = tbl_products.subCatId');
        $this->db->join('tbl_category', 'tbl_category.id = tbl_products.CatId');
        $this->db->order_by('tbl_products.sortOrder', 'DESC');
        return $this->db->get_where($this->tableName, $array)->result_array();
    }

    public function GetReviews($pName) {

        $otherdb = $this->load->database('otherdb', TRUE);

        //print_r($array);
        //$array = $pName;
        $array = array("post_title" => $pName);
        $otherdb->select("ID");
        //$this->db->join('tbl_subcategory', 'tbl_subcategory.id = tbl_products.subCatId');
        //$this->db->order_by('tbl_products.sortOrder', 'DESC');
        return $otherdb->get_where('wp_posts', $array)->result_array();
    }

    public function GetReviewsDetail($id) {

        //echo $id;

        $otherdb = $this->load->database('otherdb', TRUE);

        //print_r($array);
        //$array = $pName;
        $array = array("comment_post_ID" => $id);
        $otherdb->select("comment_content, comment_author, comment_date");
        //$this->db->join('tbl_subcategory', 'tbl_subcategory.id = tbl_products.subCatId');
        //$this->db->order_by('tbl_products.sortOrder', 'DESC');
        $res = $otherdb->get_where('wp_comments', $array)->result_array();
        $otherdb->last_query();
        return $res;
    }

    public function GetProdByCat($cid, $offset = 0, $limit = 3, $orderBy='tbl_products.sortOrder',$orderAs='DESC') {
        //print_r($array);
        $array = array('tbl_products.subCatId' => $cid);
        $this->db->select("tbl_subcategory.name As CatName, tbl_category.name AS MainCatName, tbl_products.Id,  tbl_products.CatId, tbl_products.subCatId, tbl_products.name As ProdName, tbl_products.desc, tbl_products.image, tbl_products.actualPrice, tbl_products.offeredPrice, tbl_products.sortOrder");
        $this->db->join('tbl_subcategory', 'tbl_subcategory.id = tbl_products.subCatId');
        $this->db->join('tbl_category', 'tbl_category.id = tbl_products.CatId');
        $this->db->order_by($orderBy, $orderAs);
        return $this->db->get_where($this->tableName, $array, $limit, $offset)->result_array();
    }
    
    public function GetProdByCatCount($cid, $orderBy='tbl_products.sortOrder',$orderAs='DESC') {
        //print_r($array);
        $array = array('tbl_products.subCatId' => $cid);
        $this->db->select("tbl_subcategory.name As CatName, tbl_category.name AS MainCatName, tbl_products.Id,  tbl_products.CatId, tbl_products.subCatId, tbl_products.name As ProdName, tbl_products.desc, tbl_products.image, tbl_products.actualPrice, tbl_products.offeredPrice, tbl_products.sortOrder");
        $this->db->join('tbl_subcategory', 'tbl_subcategory.id = tbl_products.subCatId');
        $this->db->join('tbl_category', 'tbl_category.id = tbl_products.CatId');
        $this->db->order_by($orderBy, $orderAs);
        $this->db->from($this->tableName);
        return $this->db->count_all_results();
        
    }

    public function AddNewReview($email, $author, $pName, $review) {

        //echo $id;

        /*echo $email . $pName . $review;
        die();*/

        // Get Product WP ID by name
        $otherdb = $this->load->database('otherdb', TRUE);

        //print_r($array);
        //$array = $pName;
        $array = array("post_title" => $pName);
        $otherdb->select("ID");
        $res = $otherdb->get_where('wp_posts', $array)->result_array();
        $otherdb->last_query();

        if(isset($res[0]['ID'])){
            $wpId = $res[0]['ID'];

            $cur_date = date("Y-m-d H:i:s");
            // NOW ADDING REVIEW TO WP DB
            $data = array(
                'comment_post_ID' => $wpId,
                'comment_author' => $author,
                'comment_author_email' => $email,
                'comment_content' => $review,
                'comment_date' => $cur_date
            );

            $res_comm = $otherdb->insert('wp_comments', $data);
        }
        else{
            $res_comm = "No Product!";
        }



        return $res_comm;




    }

    public function PlaceOrder($firstName, $lastName, $companyName = 'NA', $country, $street, $city, $state = 0, $zip = 0, $phone, $email, $payment, $productId, $productName, $productQuantity, $productPrice, $subtotal, $total, $orderNotes = 'NULL', $password, $userId = 'NULL', $guest = 'NULL', $mac = 'NULL', $size = "NULL") {

        // Get Product WP ID by name
        //$otherdb = $this->load->database('otherdb', TRUE);

        //print_r($array);
        //$array = $pName;
        $cur_date = date("Y-m-d H:i:s");

        $data = array(
            'firstName' => $firstName,
            'lastName' => $lastName,
            'companyName' => $companyName,
            'country' => $country,
            'street' => $street,
            'city' => $city,
            'state' => $state,
            'zip' => $zip,
            'phone' => $phone,
            'email' => $email,
            'payment' => $payment,
            'productId' => $productId,
            'productName' => $productName,
            'productQuantity' => $productQuantity,
            'productPrice' => $productPrice,
            'subTotal' => $subtotal,
            'total' => $total,
            'orderNotes' => $orderNotes,
            'password' => $password,
            'userId' => $userId,
            'guest' => $guest,
            'mac' => $mac,
            'size' => $size
        );

        $res_comm = $this->db->insert('tbl_orders', $data);

            $res = $this->db->insert_id();
            $res = array("Response" => "DONE", "orderNo" => $res, "date" => $cur_date, "status" => "Pending", "productId" => $productId, "productQuantity" => $productQuantity, "productName" => $productName, "productPrice" => $productPrice, "subTotal" => $subtotal, "total" => $total, "payment" => $payment, "firstName" => $firstName, "lastName" => $lastName, "city" => $city, "street" => $street, "phone" => $phone, "email" => $email, "size"=>$size);
            return $res;
        



        /*if(isset($res[0]['ID'])){
            $wpId = $res[0]['ID'];

            $cur_date = date("Y-m-d H:i:s");
            // NOW ADDING REVIEW TO WP DB
            $data = array(
                'comment_post_ID' => $wpId,
                'comment_author' => $author,
                'comment_author_email' => $email,
                'comment_content' => $review,
                'comment_date' => $cur_date
            );

            $res_comm = $otherdb->insert('wp_comments', $data);
        }
        else{
            $res_comm = "No Product!";
        }*/

        //return $res_comm;





    }

    public function get_user($array) {
        $query = $this->db->get_where($this->tableName, $array);
        return $query->result_array();
    }

    public function get_events($array) {
        //print_r($array);
        $this->db->get_where($this->tableName, $array);
        $this->db->join('tbl_bookings', 'tbl_resource_events.bookingId = tbl_bookings.BookingId');
        $query = $this->db->get_where($this->tableName, $array);
        return $query->result_array();
    }

    public function get_user_password($userId) {
        $query = $this->db->get_where($this->tableName, array('UserId' => $userId));
        return $query->result_array();
    }

    public function update_user_password($data){
        $this->db->where('UserId', $data['UserId']);
        $this->db->update("tbl_users", $data);
    }

    public function GetSearchByKey($keyword, $offset = 0, $limit = 10, $orderBy = "tbl_products.sortOrder", $orderAs = "DESC") {
        $array = array('tbl_products.name' => $keyword);
        $this->db->select("tbl_subcategory.name As CatName, tbl_category.name AS MainCatName, tbl_products.Id,  tbl_products.CatId, tbl_products.subCatId, tbl_products.name As ProdName, tbl_products.desc, tbl_products.image, tbl_products.actualPrice, tbl_products.offeredPrice, tbl_products.sortOrder");
        //$this->db->from('tbl_products');
        $this->db->join('tbl_subcategory', 'tbl_subcategory.id = tbl_products.subCatId');
        $this->db->join('tbl_category', 'tbl_category.id = tbl_products.CatId');
        $this->db->order_by($orderBy, $orderAs);
        $this->db->like('tbl_products.name', $keyword, 'both');
        $this->db->or_like('tbl_subcategory.display_name', $keyword, 'both');
        $this->db->or_like('tbl_category.name', $keyword, 'both');
        $res = $query = $this->db->get('tbl_products', $limit, $offset)->result_array();
        //$res = $this->db->get_where($this->tableName, $array, $limit, $offset)->result_array();
        //echo $this->db->last_query();
        return $res;
    }
    
    public function GetSearchByKeyCount($keyword, $orderBy = "tbl_products.sortOrder", $orderAs = "DESC") {
        $array = array('tbl_products.name' => $keyword);
        $this->db->select("tbl_subcategory.name As CatName, tbl_category.name AS MainCatName, tbl_products.Id,  tbl_products.CatId, tbl_products.subCatId, tbl_products.name As ProdName, tbl_products.desc, tbl_products.image, tbl_products.actualPrice, tbl_products.offeredPrice, tbl_products.sortOrder");
        $this->db->from('tbl_products');
        $this->db->join('tbl_subcategory', 'tbl_subcategory.id = tbl_products.subCatId');
        $this->db->join('tbl_category', 'tbl_category.id = tbl_products.CatId');
        $this->db->order_by($orderBy, $orderAs);
        $this->db->like('tbl_products.name', $keyword, 'both');
        $this->db->or_like('tbl_subcategory.display_name', $keyword, 'both');
        $this->db->or_like('tbl_category.name', $keyword, 'both');
        return $this->db->count_all_results();

    }

    public function loginUser($email, $pass) {
        $array = array('tbl_user.email' => $email, 'tbl_user.pass' => $pass);
        //$otherdb = $this->load->database('otherdb', TRUE);

        //print_r($array);
        //$array = $pName;
        $this->db->select("tbl_user.id, tbl_orders.firstName, tbl_orders.lastName, tbl_user.email, tbl_orders.city, tbl_orders.country, tbl_orders.phone, tbl_orders.street");
        $this->db->join('tbl_orders', 'tbl_orders.userId = tbl_user.id');
        $this->db->order_by('tbl_orders.id', 'DESC');
        return $this->db->get_where('tbl_user', $array)->result_array();
    }

    public function loginUserNew($email, $pass) {
        $array = array('tbl_user.email' => $email, 'tbl_user.pass' => $pass);
        //$otherdb = $this->load->database('otherdb', TRUE);

        //print_r($array);
        //$array = $pName;
        $this->db->select("tbl_user.id, '' as firstName, '' as lastName, tbl_user.email, '' as city, '' as country, '' as phone, '' as street");
        //$this->db->join('tbl_orders', 'tbl_orders.userId = tbl_user.id');
        //$this->db->order_by('tbl_orders.id', 'DESC');
        return $this->db->get_where('tbl_user', $array)->result_array();
    }

    public function loginUserDetails($userid) {
        $array = array('tbl_user.id' => $userid);

        $this->db->select("tbl_user.id, tbl_user.fname as firstName, tbl_user.lname as lastName, tbl_user.email, tbl_user.city as city, tbl_user.country as country, tbl_user.phone as phone, tbl_user.street as street");

        return $this->db->get_where('tbl_user', $array)->result_array();
    }

    public function GetOrderById($userId) {
        //print_r($array);
        $array = $userId;
        $this->db->select("tbl_orders.id, tbl_orders.firstName, tbl_orders.lastName, tbl_orders.companyName, tbl_orders.street, tbl_orders.city, tbl_orders.state, tbl_orders.zip, tbl_orders.phone, tbl_orders.email, tbl_orders.payment, tbl_orders.productId, tbl_orders.productName, tbl_orders.productQuantity, tbl_orders.productPrice, tbl_orders.subTotal, tbl_orders.total, DATE_FORMAT(tbl_orders.dtime,'%b %d,%Y') as dtime, tbl_orders.orderNotes, tbl_orders.status");
        //$this->db->join('tbl_subcategory', 'tbl_subcategory.id = tbl_products.subCatId');
        //$this->db->join('tbl_category', 'tbl_category.id = tbl_products.CatId');
        $this->db->order_by('tbl_orders.id', 'DESC');
        return $this->db->get_where('tbl_orders', $array)->result_array();
    }

    public function TrackOrder($orderId) {
        //print_r($array);
        $array = $orderId;
        $this->db->select("tbl_orders.firstName, tbl_orders.lastName, tbl_orders.companyName, tbl_orders.street, tbl_orders.city, tbl_orders.state, tbl_orders.zip, tbl_orders.phone, tbl_orders.email, tbl_orders.payment, tbl_orders.productId, tbl_orders.productName, tbl_orders.productQuantity, tbl_orders.productPrice, tbl_orders.subTotal, tbl_orders.total, tbl_orders.dtime, tbl_orders.orderNotes");
        //$this->db->join('tbl_subcategory', 'tbl_subcategory.id = tbl_products.subCatId');
        //$this->db->join('tbl_category', 'tbl_category.id = tbl_products.CatId');
        $this->db->order_by('tbl_orders.id', 'DESC');
        return $this->db->get_where('tbl_orders', $array)->result_array();
    }

    public function NewUserRegister($email, $pass) {

        $array = array("email" => $email, "pass" => $pass);
        $this->db->select("id");
        $res = $this->db->get_where('tbl_user', $array)->result_array();

        if(!empty($res)){
            $cur_date = date("Y-m-d H:i:s");
            // NOW ADDING REVIEW TO WP DB
            $res = array('Status' => 'Failed', 'Response' => 'Email already exist!');
        }
        else{
            $res_comm = $this->db->insert('tbl_user', $array);

            if(!empty($res_comm)){
                $uid = $this->db->insert_id();
                $res = array('Status' => 'Success', 'Response' => $uid);
            }
            else{
                $res = array('Status' => 'Failed', 'Response' => 'Registration Failed!');
            }
        }
        //return $res_comm;
        return $res;
    }

    public function GetUserDetails($userId) {
        //print_r($array);
        $array = $userId;
        $this->db->select("tbl_orders.firstName, tbl_orders.lastName, tbl_orders.country, tbl_orders.street, tbl_orders.city, tbl_orders.phone, tbl_orders.email");
        //$this->db->join('tbl_subcategory', 'tbl_subcategory.id = tbl_products.subCatId');
        //$this->db->join('tbl_category', 'tbl_category.id = tbl_products.CatId');
        $this->db->order_by('tbl_orders.id', 'DESC');
        return $this->db->get_where('tbl_orders', $array, 1)->result_array();
    }

    public function ContactUs($type, $name, $contactNumber, $email, $organization, $message) {

        $array = array("type" => $type, "name" => $name, "contact" => $contactNumber, "email" => $email, "organization" => $organization, "message" => $message);

        $res_comm = $this->db->insert('tbl_contactUs', $array);
        $uid = $this->db->insert_id();
        $res = array('Status' => 'Success', 'Response' => $uid);

        //return $res_comm;
        return $res;
    }
    
    public function variable_prices($data, $limit = 0, $offset = 0){
        $this->db->select('tbl_variant.name as type, tbl_product_variation_map.price as price');
        $this->db->join('tbl_variant', 'tbl_variant.id = tbl_product_variation_map.id');
        return $this->db->get_where('tbl_product_variation_map',$data, $limit, $offset)->result_array();
        
    }

}
