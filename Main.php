<?php

date_default_timezone_set('UTC');


defined('BASEPATH') or exit('No direct script access allowed');

$autoloader = dirname(__FILE__) . '/vendor/autoload.php';
if (is_readable($autoloader)) {
    require_once $autoloader;
}

use Automattic\WooCommerce\Client;
use Automattic\WooCommerce\HttpClient\HttpClientException;

class Main extends VPA_Controller
{

    public $apiResponse;
    public $woo;
    protected $greene_services_url = "https://www.greene.pk/services/";
    protected $greene_url = 'https://www.greene.pk';

    public function __construct()
    {
        parent::__construct();
        $this->apiResponse = array('Response' => 'Invalid API Request');
        $this->woo = new Client(
            $this->greene_url,
            'ck_9a967c28a0a6e4fcb9b2451831cc92c617cee0dc',
            'cs_1ada29b58b2464b03d0b7c62937fc652e88a43ae',
            [
                'wp_api' => true,
                'version' => 'wc/v2',
                'query_string_auth' => true
            ]
        );
    }

    public function index()
    {
        $request = $this->input->get('request');
        switch ($request):
            case "HomeContent":
                $this->apiResponse = $this->HomeContent();
                break;
            case "HomeContent1":
                $this->apiResponse = $this->HomeContent1();
                break;
            case "GetProdById":
                $this->apiResponse = $this->GetProdById();
                break;
            case "GetProdById1":
                $this->apiResponse = $this->GetProdById1();
                break;
            case "GetProdByCat":
                $this->apiResponse = $this->GetProdByCat();
                break;
            case "GetReviews":
                $this->apiResponse = $this->GetReviews();
                break;
            case "GetSearchByKey":
                $this->apiResponse = $this->GetSearchByKey();
                break;
            case "AddNewReview":
                $this->apiResponse = $this->AddNewReview();
                break;
            case "loginUser":
                $this->apiResponse = $this->loginUser();
                break;
            case "loginUserNew":
                $this->apiResponse = $this->loginUserNew();
                break;
            case "GetOrderById":
                $this->apiResponse = $this->GetOrderById();
                break;
            case "GetUserDetails":
                $this->apiResponse = $this->GetUserDetails();
                break;
            case "TrackOrder":
                $this->apiResponse = $this->TrackOrder();
                break;
            case "PlaceOrder":
                $this->apiResponse = $this->PlaceOrder();
                break;
            case "NewUserRegister":
                $this->apiResponse = $this->NewUserRegister();
                break;
            case "ContactUs":
                $this->apiResponse = $this->ContactUs();
                break;
            case "ContactUsMob":
                $this->apiResponse = $this->ContactUsMob();
                break;
            case "ContactUsNew":
                $this->apiResponse = $this->ContactUsNew();
                break;
            case "register":
                $this->apiResponse = $this->register();
                break;
            case "login":
                $this->apiResponse = $this->login();
                break;
            case "GetEvents":
                $this->apiResponse = $this->GetEventsById();
                break;
            case "password-recovery-code":
                $this->apiResponse = $this->get_password_recovery_secret_code();
                break;
            case "reset-password":
                $this->apiResponse = $this->reset_password();
                break;
            case "change-password":
                $this->apiResponse = $this->change_password();
                break;
            case "get-users":
                $this->apiResponse = $this->get_users();
                break;
            case 'get-profile':
                $this->apiResponse = $this->get_profile();
                break;
            case 'set-profile':
                $this->apiResponse = $this->set_profile();
                break;
            case 'booking':
                $this->apiResponse = $this->booking();
                break;
            case 'edit-booking':
                $this->apiResponse = $this->edit_booking();
                break;
            case 'history':
                $this->apiResponse = $this->history();
                break;
            case 'add-payment-method':
                $this->apiResponse = $this->add_payment_method();
                break;
            case 'get-payment-methods':
                $this->apiResponse = $this->get_payment_methods();
                break;
            case 'add-query':
                $this->apiResponse = $this->add_query();
                break;
            case 'get-queries':
                $this->apiResponse = $this->get_queries();
                break;
            case 'edit-query':
                $this->apiResponse = $this->edit_query();
                break;
            case 'get-app-info':
                $this->apiResponse = $this->get_app_info();
                break;
            case 'edit-app-info':
                $this->apiResponse = $this->edit_app_info();
                break;
            case 'fcm':
                $this->apiResponse = $this->add_fcm_mac();
                break;
            case 'get-notifications':
                $this->apiResponse = $this->get_notifications();
                break;
            case 'newsletter':
                $this->apiResponse = $this->newsletter();
                break;
            case 'change-password':
                $this->apiResponse = $this->change_password();
                break;
            case 'edit-user':
                $this->apiResponse = $this->edit_user();
                break;
            case 'forgot-pass':
                $this->apiResponse = $this->forgotPassword();
                break;
            case 'auth':
                $this->apiResponse = $this->greeni_auth();
                break;
            case 'signup':
                $this->apiResponse = $this->greeni_signup();
                break;
            case 'create-customer':
                $this->apiResponse = $this->greeni_create_customer();
                break;
            case 'get-customer':
                $this->apiResponse = $this->greeni_get_customer();
                break;
            case 'update-customer':
                $this->apiResponse = $this->greeni_update_user();
                break;
            case 'create-order':
                $this->apiResponse = $this->greeni_create_order();
                break;
            case 'get-orders':
                $this->apiResponse = $this->greeni_get_orders();
                break;
            case 'get-orders-emails':
                $this->apiResponse = $this->greeni_get_orders_emails();
                break;
            case 'get-products':
                $this->apiResponse = $this->greeni_get_products();
                break;
            case 'get-products-cron':
                $this->apiResponse = $this->greeni_get_products2();
                break;
            case 'get-products-by-id':
                $this->apiResponse = $this->greeni_get_products_by_id();
                break;
            case 'get-payment-gateways':
                $this->apiResponse = $this->greeni_get_payment_gateways();
                break;
            case 'reset-pass':
                $this->apiResponse = $this->greeni_reset_password();
                break;
            case 'get-product-variations':
                $this->apiResponse = $this->greeni_get_product_variations();
                break;
            case 'get-categories':
                $this->apiResponse = $this->greeni_get_product_categories();
                break;
            case 'get-home-content':
                $this->apiResponse = $this->greeni_get_home_content();
                break;
            case 'add-product-review':
                $this->apiResponse = $this->greeni_add_product_review();
                break;
            case 'cron-home-content':
                $this->apiResponse = $this->_cron_greeni_write_home_content();
                break;
            default:
                //$this->apiResponse = $this->get_app_info();
                $this->apiResponse = $this->HomeContent();
                break;
        endswitch;

        echo json_encode($this->apiResponse);
        exit;
    }

    public function register()
    {

        $password = password_hash($this->input->get('password'), PASSWORD_DEFAULT);

        $data['user'] = array(
            'UserId' => '',
            'Email' => $this->input->get('email'),
            'Password' => $password,
            'UserType' => $this->input->get('userType'),
            'AppId' => $this->input->get('appId'),
            'UserStatus' => 1
        );

        $response = $this->User_Model->register($data['user']);

        if ($response) {

            $response = $this->User_Model->get_user(array('Email' => $this->input->get('email')));
            $userId = $response[0]['UserId'];
            $data['profile'] = array(
                'ProfileId' => '',
                'UserId' => $userId,
                'Name' => $this->input->get('name'),
                'Email' => $this->input->get('email'),
                'Address' => $this->input->get('address'),
                'MobileNo' => $this->input->get('mobileNo'),
                'ProfileImage' => $this->input->get('profileImage')
            );

            $response = $this->Profile_Model->add_profile($data['profile']);
            return array('Response' => "User registered successfully", 'UserId' => $userId);
        } else {
            return array('Response' => "Email already exists", 'UserId' => '');
        }

        return $this->apiResponse;
    }

    public function login()
    {

        $password = $this->input->get('password');
        $email = $this->input->get('email');
        $appId = $this->input->get('appId');

        if ($this->input->get('userType') == 1) {
            $array = $this->User_Model->get_user(array('Email' => $email, 'UserStatus' => 1));
        } else {
            $array = $this->User_Model->get_user(array('Email' => $email, 'UserStatus' => 1, 'AppId' => $appId));
        }

        if (!empty($array)) {
            $hash = $array[0]['Password'];
            if (password_verify($password, $hash)) {
                return array('Response' => 'Success', 'UserId' => $array[0]['UserId'], 'UserType' => $array[0]['UserType']);
            } else {
                return array('Response' => 'Incorrect Password', 'UserId' => '');
            }
        } else {
            return array('Response' => 'Invalid User', 'UserId' => '');
        }

        return $this->apiResponse;
    }

    public function HomeContent()
    {

        //$uid = $this->input->get('uid');
        /* $email = $this->input->get('email');
          $appId = $this->input->get('appId'); */

        $array = $this->Product_Model->GetSlider();

        /* echo "<pre>";
          print_r($array); */

        if (!empty($array)) {
            //$hash = $array[0]['id'];
            //if (isset($hash) > "0") {
            $i = 0;

            foreach ($array as $Arr) {
                $image = $this->greene_services_url . "images/" . $Arr['MainCatName'] . "/" . $Arr['CatName'] . "/200x200/" . strtolower($Arr['ProdName']) . ".jpg";
                $res["Response"]['Slider'][$i] = array('id' => $Arr['Id'], 'Category' => $Arr['CatId'], 'SubCategory' => $Arr['subCatId'], 'Name' => $Arr['ProdName'], 'Desc' => $Arr['desc'], 'Image' => $image, 'actualPrice' => $Arr['actualPrice'], 'offeredPrice' => $Arr['offeredPrice'], 'isClickable' => $Arr['isClickable']);
                $i++;
            }

            $i = 0;
            $array = $this->Product_Model->GetAllParentCategories();

            /* echo "<pre>";
              print_r($array); */

            foreach ($array as $Arr) {
                $res["Response"]['Category'][$i] = array('id' => $Arr['id'], 'name' => $Arr['name'], 'status' => $Arr['status']);
                //$res["Response"]['Category'][$i] = $Arr['name'];
                $i++;
            }




            $i = 0;
            $array = $this->Product_Model->GetAllCategories();

            /* echo "<pre>";
              print_r($array); */

            foreach ($array as $Arr) {
                $res["Response"]['subCategory'][$i] = array('ParentCatId' => $Arr['cat_id'], 'id' => $Arr['id'], 'CategoryName' => $Arr['name'], 'CategoryDisplayName' => $Arr['display_name'], 'order' => $Arr['order']);
                //$res["Response"]['Category'][$i] = $Arr['name'];
                $i++;
            }


            // Get 4 products of each category
            $tot_cat = count($res["Response"]['subCategory']);

            for ($j = 0; $j < $tot_cat; $j++) {
                $i = 0;
                $array = $this->Product_Model->HomeProducts($catId = $res["Response"]['subCategory'][$j]["id"]);

                /* echo "<pre>";
                  print_r($array); */

                foreach ($array as $Arr) {
                    $image = $this->greene_services_url . "images/" . $Arr['MainCatName'] . "/" . $Arr['CatName'] . "/200x200/" . strtolower($Arr['ProdName']) . ".jpg";
                    $res["Response"][$Arr['CatName']][$i] = array('id' => $Arr['Id'], 'Category' => $Arr['CatId'], 'CategoryName' => $Arr['CatName'], 'SubCategory' => $Arr['subCatId'], 'Name' => $Arr['ProdName'], 'Desc' => $Arr['desc'], 'Image' => $image, 'actualPrice' => $Arr['actualPrice'], 'offeredPrice' => $Arr['offeredPrice'], 'order' => $Arr['sortOrder']);
                    $i++;
                }
            }



            return $res;

            //return array('id' => 'Success', 'userId' => $array[0]['userId']);
            /* } else {
              return array('Response' => 'No Resource');
              } */
        } else {
            return array('Response' => 'Invalid User');
        }

        return $this->apiResponse;
    }

    public function HomeContent1()
    {

        //$uid = $this->input->get('uid');
        /* $email = $this->input->get('email');
          $appId = $this->input->get('appId'); */

        $array = $this->Product_Model->GetSlider();

        /* echo "<pre>";
          print_r($array); */

        if (!empty($array)) {
            //$hash = $array[0]['id'];
            //if (isset($hash) > "0") {
            $i = 0;

            foreach ($array as $Arr) {
                echo $image = base_url() . "images/" . $Arr['MainCatName'] . "/" . $Arr['CatName'] . "/200x200/" . $Arr['ProdName'] . ".jpg";
                echo "<br>";
                $res["Response"]['Slider'][$i] = array('id' => $Arr['Id'], 'Category' => $Arr['CatId'], 'SubCategory' => $Arr['subCatId'], 'Name' => $Arr['ProdName'], 'Desc' => $Arr['desc'], 'Image' => $image, 'actualPrice' => $Arr['actualPrice'], 'offeredPrice' => $Arr['offeredPrice'], 'isClickable' => $Arr['isClickable']);
                $i++;
            }
            exit;
            $i = 0;
            $array = $this->Product_Model->GetAllParentCategories();

            /* echo "<pre>";
              print_r($array); */

            foreach ($array as $Arr) {
                $res["Response"]['Category'][$i] = array('id' => $Arr['id'], 'name' => $Arr['name'], 'status' => $Arr['status']);
                //$res["Response"]['Category'][$i] = $Arr['name'];
                $i++;
            }




            $i = 0;
            $array = $this->Product_Model->GetAllCategories();

            /* echo "<pre>";
              print_r($array); */

            foreach ($array as $Arr) {
                $res["Response"]['subCategory'][$i] = array('ParentCatId' => $Arr['cat_id'], 'id' => $Arr['id'], 'CategoryName' => $Arr['name'], 'CategoryDisplayName' => $Arr['display_name'], 'order' => $Arr['order']);
                //$res["Response"]['Category'][$i] = $Arr['name'];
                $i++;
            }


            // Get 4 products of each category
            $tot_cat = count($res["Response"]['subCategory']);

            for ($j = 0; $j < $tot_cat; $j++) {
                $i = 0;
                $array = $this->Product_Model->HomeProducts($catId = $res["Response"]['subCategory'][$j]["id"]);

                /* echo "<pre>";
                  print_r($array); */

                foreach ($array as $Arr) {
                    $image = $this->greene_services_url . "images/" . $Arr['MainCatName'] . "/" . $Arr['CatName'] . "/200x200/" . strtolower($Arr['ProdName']) . ".jpg";
                    $res["Response"][$Arr['CatName']][$i] = array('id' => $Arr['Id'], 'Category' => $Arr['CatId'], 'CategoryName' => $Arr['CatName'], 'SubCategory' => $Arr['subCatId'], 'Name' => $Arr['ProdName'], 'Desc' => $Arr['desc'], 'Image' => $image, 'actualPrice' => $Arr['actualPrice'], 'offeredPrice' => $Arr['offeredPrice'], 'order' => $Arr['sortOrder']);
                    $i++;
                }
            }



            return $res;

            //return array('id' => 'Success', 'userId' => $array[0]['userId']);
            /* } else {
              return array('Response' => 'No Resource');
              } */
        } else {
            return array('Response' => 'Invalid User');
        }

        return $this->apiResponse;
    }

    public function GetProdById()
    {

        $limit = 10;

        if ($this->input->get('limit') != "") {
            $limit = $this->input->get('limit');
        }

        if ($this->input->get('name') != "") {
            $name = str_replace("-", "_", $this->input->get('name'));
            $array = $this->Product_Model->GetProdById(array('tbl_products.name' => $name));
        }

        if ($this->input->get('pid') != "") {
            $uid = $this->input->get('pid');
            $array = $this->Product_Model->GetProdById(array('tbl_products.Id' => $uid));
        }


        if (!empty($array)) {
            $hash = $array[0]['Id'];
            if (isset($hash) > "0") {
                $i = 0;
                foreach ($array as $Arr) {
                    $image = $this->greene_services_url . "images/" . $Arr['MainCatName'] . "/" . $Arr['CatName'] . "/200x200/" . strtolower($Arr['ProdName']) . ".jpg";
                    $res["Response"][$i] = array('id' => $Arr['Id'], 'Category' => $Arr['CatId'], 'CategoryName' => $Arr['CatName'], 'SubCategory' => $Arr['subCatId'], 'Name' => $Arr['ProdName'], 'Desc' => str_replace(array("\\r\\n", "\\r", "\\n"), PHP_EOL, $Arr['desc']), 'Image' => $image, 'actualPrice' => $Arr['actualPrice'], 'offeredPrice' => $Arr['offeredPrice'], 'order' => $Arr['sortOrder']);
                    $i++;
                }


                //$i = 0;
                $array = $this->Product_Model->HomeProducts($catId = $Arr['subCatId'], $limit);

                /* echo "<pre>";
                  print_r($array); */

                foreach ($array as $Arr) {
                    if ($res["Response"][0]['id'] == $Arr['Id']) {
                    } else {
                        $image = $this->greene_services_url . "images/" . $Arr['MainCatName'] . "/" . $Arr['CatName'] . "/200x200/" . strtolower($Arr['ProdName']) . ".jpg";
                        $res["Response"][$i] = array('id' => $Arr['Id'], 'Category' => $Arr['CatId'], 'CategoryName' => $Arr['CatName'], 'SubCategory' => $Arr['subCatId'], 'Name' => $Arr['ProdName'], 'Desc' => str_replace(array("\\r\\n", "\\r", "\\n"), PHP_EOL, $Arr['desc']), 'Image' => $image, 'actualPrice' => $Arr['actualPrice'], 'offeredPrice' => $Arr['offeredPrice'], 'order' => $Arr['sortOrder']);
                        $i++;
                    }
                }

                $i = 0;
                $array = $this->GetReviews($res['Response'][0]['Name']);

                /* echo "<pre>";
                  print_r($array); */

                if (isset($array[0]['review'])) {
                    $res["Reviews"] = $array;
                    /* foreach($array as $Arr){
                      $res["Review"][$i] = array('review' => $Arr['review'], 'author' => $Arr['author']);
                      $i++;
                      } */
                }




                return $res;

                //return array('id' => 'Success', 'userId' => $array[0]['userId']);
            } else {
                return array('Response' => 'No Resource');
            }
        } else {
            return array('Response' => 'Invalid User');
        }

        return $this->apiResponse;
    }

    public function GetProdById1()
    {

        $limit = 10;

        if ($this->input->get('limit') != "") {
            $limit = $this->input->get('limit');
        }

        if ($this->input->get('name') != "") {
            $name = str_replace("-", "_", $this->input->get('name'));
            $array = $this->Product_Model->GetProdById(array('tbl_products.name' => $name));
        }

        if ($this->input->get('pid') != "") {
            $uid = $this->input->get('pid');
            $array = $this->Product_Model->GetProdById(array('tbl_products.Id' => $uid));
        }


        if (!empty($array)) {
            $hash = $array[0]['Id'];
            if (isset($hash) > "0") {
                $i = 0;
                foreach ($array as $Arr) {
                    $image = $this->greene_services_url . "images/" . $Arr['MainCatName'] . "/" . $Arr['CatName'] . "/200x200/" . strtolower($Arr['ProdName']) . ".jpg";
                    $res["Response"][$i] = array('id' => $Arr['Id'], 'Category' => $Arr['CatId'], 'CategoryName' => $Arr['CatName'], 'SubCategory' => $Arr['subCatId'], 'Name' => $Arr['ProdName'], 'Desc' => str_replace(array("\\r\\n", "\\r", "\\n"), PHP_EOL, $Arr['desc']), 'Image' => $image, 'actualPrice' => $Arr['actualPrice'], 'offeredPrice' => $Arr['offeredPrice'], 'order' => $Arr['sortOrder']);
                    $res["Response"][$i]['VariablePrices'] = $this->Product_Model->variable_prices(array('pId' => $Arr['Id']));
                    array_push($res["Response"][$i]['VariablePrices'], array('type' => 'Standard', 'price' => $Arr['actualPrice']));
                    $i++;
                }


                //$i = 0;
                $array = $this->Product_Model->HomeProducts($catId = $Arr['subCatId'], $limit);

                /* echo "<pre>";
                  print_r($array); */

                foreach ($array as $Arr) {
                    if ($res["Response"][0]['id'] == $Arr['Id']) {
                    } else {
                        $image = $this->greene_services_url . "images/" . $Arr['MainCatName'] . "/" . $Arr['CatName'] . "/200x200/" . strtolower($Arr['ProdName']) . ".jpg";
                        $res["Response"][$i] = array('id' => $Arr['Id'], 'Category' => $Arr['CatId'], 'CategoryName' => $Arr['CatName'], 'SubCategory' => $Arr['subCatId'], 'Name' => $Arr['ProdName'], 'Desc' => str_replace(array("\\r\\n", "\\r", "\\n"), PHP_EOL, $Arr['desc']), 'Image' => $image, 'actualPrice' => $Arr['actualPrice'], 'offeredPrice' => $Arr['offeredPrice'], 'order' => $Arr['sortOrder']);
                        $res["Response"][$i]['VariablePrices'] = $this->Product_Model->variable_prices(array('pId' => $Arr['Id']));
                        array_push($res["Response"][$i]['VariablePrices'], array('type' => 'Standard', 'price' => $Arr['actualPrice']));

                        $i++;
                    }
                }

                $i = 0;
                $array = $this->GetReviews($res['Response'][0]['Name']);

                /* echo "<pre>";
                  print_r($array); */

                if (isset($array[0]['review'])) {
                    $res["Reviews"] = $array;
                    /* foreach($array as $Arr){
                      $res["Review"][$i] = array('review' => $Arr['review'], 'author' => $Arr['author']);
                      $i++;
                      } */
                }




                return $res;

                //return array('id' => 'Success', 'userId' => $array[0]['userId']);
            } else {
                return array('Response' => 'No Resource');
            }
        } else {
            return array('Response' => 'Invalid User');
        }

        return $this->apiResponse;
    }

    public function GetProdByCat()
    {
        $response = array('Response' => 'No Item found!');

        $uid = $this->input->get('cid');
        $offset = $this->input->get('offset');
        $limit = $this->input->get('limit');
        $orderBy = $this->input->get('orderBy');
        $orderAs = $this->input->get('orderAs');

        if ($orderAs == "") {
            $orderAs = "DESC";
        }

        switch ($orderBy):
            case "high":
                $orderBy = "tbl_products.offeredPrice";
                $orderAs = "DESC";
                break;
            case "low":
                $orderBy = "tbl_products.offeredPrice";
                $orderAs = "ASC";
                break;
            case "new":
                $orderBy = "tbl_products.Id";
                $orderAs = "DESC";
                break;
            default:
                $orderBy = "tbl_products.sortOrder";
                break;
        endswitch;

        //        echo $resultCount = $this->Product_Model->GetProdByCatCount($uid, $orderBy, $orderAs);
        //
        //         if($resultCount > $limit){
        //            $offset = $resultCount - $limit;
        //            
        //        }else{
        //            $offset = 0;
        //        }

        $i = 0;
        $array = $this->Product_Model->GetProdByCat($uid, $offset, $limit, $orderBy, $orderAs);

        if (!empty($array) < 1) {
            $res["Response"][$i] = "No Item found!";
        } else {
            foreach ($array as $Arr) {
                $image = $this->greene_services_url . "images/" . $Arr['MainCatName'] . "/" . $Arr['CatName'] . "/200x200/" . strtolower($Arr['ProdName']) . ".jpg";
                $res["Response"][$i] = array('id' => $Arr['Id'], 'Category' => $Arr['CatId'], 'CategoryName' => $Arr['CatName'], 'SubCategory' => $Arr['subCatId'], 'Name' => $Arr['ProdName'], 'Desc' => $Arr['desc'], 'Image' => $image, 'actualPrice' => $Arr['actualPrice'], 'offeredPrice' => $Arr['offeredPrice'], 'order' => $Arr['sortOrder']);
                $i++;
            }
        }
        return $res;
    }

    public function GetSearchByKey()
    {
        $keyword = $this->input->get('keyword');
        $offset = $this->input->get('offset');
        $limit = $this->input->get('limit');
        $orderBy = $this->input->get('orderBy');
        $orderAs = $this->input->get('orderAs');

        if ($orderAs == "") {
            $orderAs = "DESC";
        }

        switch ($orderBy):
            case "high":
                $orderBy = "tbl_products.offeredPrice";
                $orderAs = "DESC";
                break;
            case "low":
                $orderBy = "tbl_products.offeredPrice";
                $orderAs = "ASC";
                break;
            case "new":
                $orderBy = "tbl_products.Id";
                $orderAs = "DESC";
                break;
            default:
                $orderBy = "tbl_products.sortOrder";
                break;
        endswitch;


        //        $searchCount = $this->Product_Model->GetSearchByKeyCount($keyword, $orderBy, $orderAs);
        //        if($searchCount > $limit){
        //            $offset = $searchCount - $limit;
        //            
        //        }else{
        //            $offset = 0;
        //        }


        $i = 0;
        $array = $this->Product_Model->GetSearchByKey($keyword, $offset, $limit, $orderBy, $orderAs);

        /* echo "<pre>";
          print_r($array); */

        if (count($array) < 1) {
            $res["Response"][$i] = "No Item found!";
        } else {
            foreach ($array as $Arr) {
                $image = $this->greene_services_url . "images/" . $Arr['MainCatName'] . "/" . $Arr['CatName'] . "/200x200/" . strtolower($Arr['ProdName']) . ".jpg";
                $res["Response"][$i] = array('id' => $Arr['Id'], 'Category' => $Arr['CatId'], 'CategoryName' => $Arr['CatName'], 'SubCategory' => $Arr['subCatId'], 'Name' => $Arr['ProdName'], 'Desc' => $Arr['desc'], 'Image' => $image, 'actualPrice' => $Arr['actualPrice'], 'offeredPrice' => $Arr['offeredPrice'], 'order' => $Arr['sortOrder']);
                $i++;
            }
        }
        return $res;
    }

    public function GetReviews($pName)
    {
        $uid = $pName;
        //$uid = $this->input->get('pName');
        //$offset = $this->input->get('offset');
        //$limit = $this->input->get('limit');

        $i = 0;
        $array = $this->Product_Model->GetReviews($uid);

        /* echo "<pre>";
          print_r($array); */

        $res = array();
        if (!empty($array)) {
            $array = $this->Product_Model->GetReviewsDetail($array[0]['ID']);

            /* echo "<pre>";
              print_r($array); */

            foreach ($array as $Arr) {
                $res[$i] = array('review' => $Arr['comment_content'], 'author' => $Arr['comment_author'], 'dtime' => $Arr['comment_date']);
                $i++;
            }
        }
        return $res;
    }

    public function AddNewReview()
    {
        //$uid = $pName;
        $email = $this->input->get('email');
        $pName = str_replace("_", " ", $this->input->get('pName'));
        $review = str_replace("_", " ", $this->input->get('review'));
        $author = str_replace("_", " ", $this->input->get('author'));
        //$offset = $this->input->get('offset');
        //$limit = $this->input->get('limit');



        $i = 0;
        $array = $this->Product_Model->AddNewReview($email, $author, $pName, $review);

        if (!empty($array)) {
            $res[$i] = array('Response' => 'DONE');
        } else {
            $res[$i] = array('Response' => 'Failed');
        }

        /* echo "<pre>";
          print_r($array);
          die();

          $res = array();
          if(!empty($array)){
          $array = $this->Product_Model->GetReviewsDetail($array[0]['ID']);

          /*echo "<pre>";
          print_r($array);

          foreach($array as $Arr){
          $res[$i] = array('review' => $Arr['comment_content'], 'author' => $Arr['comment_author']);
          $i++;
          }
          } */
        return $res;
    }

    public function loginUser()
    {
        //$uid = $pName;
        $email = $this->input->get('email');
        $pass = $this->input->get('pass');
        //$offset = $this->input->get('offset');
        //$limit = $this->input->get('limit');

        $i = 0;
        $array = $this->Product_Model->loginUser($email, $pass);

        /* echo "<pre>";
          print_r($array); */

        if (!empty($array)) {
            $res[$i] = array(
                'Response' => 'Success',
                'Id' => $array[0]['id'],
                'fname' => ($array[0]['firstName'] == null) ? "" : $array[0]['firstName'],
                'lname' => ($array[0]['lastName'] == null) ? "" : $array[0]['lastName'],
                'email' => ($array[0]['email'] == null) ? "" : $array[0]['email'],
                'city' => ($array[0]['city'] == null) ? "" : $array[0]['city'],
                'country' => ($array[0]['country'] == null) ? "" : $array[0]['country'],
                'phone' => ($array[0]['phone'] == null) ? "" : $array[0]['phone'],
                'street' => ($array[0]['street'] == null) ? "" : $array[0]['street']
            );
        } else {
            $array = $this->Product_Model->loginUserNew($email, $pass);
            if (!empty($array)) {
                $res[$i] = array(
                    'Response' => 'Success',
                    'Id' => $array[0]['id'],
                    'fname' => ($array[0]['firstName'] == null) ? "" : $array[0]['firstName'],
                    'lname' => ($array[0]['lastName'] == null) ? "" : $array[0]['lastName'],
                    'email' => ($array[0]['email'] == null) ? "" : $array[0]['email'],
                    'city' => ($array[0]['city'] == null) ? "" : $array[0]['city'],
                    'country' => ($array[0]['country'] == null) ? "" : $array[0]['country'],
                    'phone' => ($array[0]['phone'] == null) ? "" : $array[0]['phone'],
                    'street' => ($array[0]['street'] == null) ? "" : $array[0]['street']
                );
            } else {
                $res[$i] = array('Response' => 'Failed');
            }
        }

        return $res;
    }

    public function loginUserNew()
    {
        //$uid = $pName;
        $email = $this->input->get('email');
        $pass = $this->input->get('pass');
        //$offset = $this->input->get('offset');
        //$limit = $this->input->get('limit');

        $i = 0;
        $array = $this->Product_Model->loginUserNew($email, $pass);

        /* echo "<pre>";
          print_r($array); */

        if (!empty($array)) {
            $res[$i] = array(
                'Response' => 'Success',
                'Id' => $array[0]['id'],
                'fname' => ($array[0]['firstName'] == null) ? "" : $array[0]['firstName'],
                'lname' => ($array[0]['lastName'] == null) ? "" : $array[0]['lastName'],
                'email' => ($array[0]['email'] == null) ? "" : $array[0]['email'],
                'city' => ($array[0]['city'] == null) ? "" : $array[0]['city'],
                'country' => ($array[0]['country'] == null) ? "" : $array[0]['country'],
                'phone' => ($array[0]['phone'] == null) ? "" : $array[0]['phone'],
                'street' => ($array[0]['street'] == null) ? "" : $array[0]['street']
            );
        } else {
            $res[$i] = array('Response' => 'Failed');
        }

        return $res;
    }

    public function GetOrderById()
    {
        $uid = $this->input->get('userId');
        $array = $this->Product_Model->GetOrderById(array('tbl_orders.userId' => $uid));
        /* echo "<pre>";
          print_r($array);
          die(); */
        if (!empty($array)) {
            $hash = '1';
            if (isset($hash) > "0") {
                $i = 0;
                foreach ($array as $Arr) {
                    $res["Response"][$i] = $Arr;
                    $i++;
                }
                return $res;
                //return array('id' => 'Success', 'userId' => $array[0]['userId']);
            } else {
                return array('Response' => 'No Resource');
            }
        } else {
            $res["Response"][0] = 'No order found!';
            return $res;
        }

        return $this->apiResponse;
    }

    public function TrackOrder()
    {
        $uid = $this->input->get('Id');
        $email = $this->input->get('email');
        $array = $this->Product_Model->TrackOrder(array('tbl_orders.id' => $uid, 'tbl_orders.email' => $email));
        /* echo "<pre>";
          print_r($array);
          die(); */
        if (!empty($array)) {
            $hash = '1';
            if (isset($hash) > "0") {
                $i = 0;
                foreach ($array as $Arr) {
                    $res["Response"][$i] = $Arr;
                    $i++;
                }
                return $res;
                //return array('id' => 'Success', 'userId' => $array[0]['userId']);
            } else {
                return array('Response' => 'No Resource');
            }
        } else {
            return array('Response' => 'Invalid User');
        }

        return $this->apiResponse;
    }

    public function PlaceOrder()
    {
        //$uid = $pName;
        $firstName = $this->input->get('firstName');
        $lastName = $this->input->get('lastName');
        $companyName = $this->input->get('companyName');
        $country = $this->input->get('country');
        $street = $this->input->get('street');
        $city = $this->input->get('city');
        $state = $this->input->get('state');
        $zip = $this->input->get('zip');
        $phone = $this->input->get('phone');
        $email = $this->input->get('email');
        $payment = $this->input->get('payment');
        $subtotal = $this->input->get('subtotal');
        $total = $this->input->get('total');
        $productId = $this->input->get('productId');    // comma separated
        $productName = $this->input->get('productName');
        $productQuantity = $this->input->get('productQuantity');
        $productPrice = $this->input->get('productPrice');
        $orderNotes = $this->input->get('orderNotes');
        $password = $this->input->get('password');
        $userId = $this->input->get('userId');
        $guest = $this->input->get('guest');
        $mac = $this->input->get('mac');
        $size = $this->input->get('size');
        /* $pName = str_replace("_", " ", $this->input->get('pName'));
          $review = str_replace("_", " ", $this->input->get('review'));
          $author = str_replace("_", " ", $this->input->get('author')); */
        //$offset = $this->input->get('offset');
        //$limit = $this->input->get('limit');



        $i = 0;

        if ($guest == '1') {              // Guest user
            $array = $this->Product_Model->PlaceOrder($firstName, $lastName, $companyName, $country, $street, $city, $state, $zip, $phone, $email, $payment, $productId, $productName, $productQuantity, $productPrice, $subtotal, $total, $orderNotes, $password, $userId, $guest, $mac, $size);
        } else if ($guest == '2') {         // New User
            $createUser = $this->NewUserRegister();

            if (isset($createUser['Status'])) {
                if ($createUser['Status'] == 'Success') {
                    $userId = $createUser['Response'];
                    $array = $this->Product_Model->PlaceOrder($firstName, $lastName, $companyName, $country, $street, $city, $state, $zip, $phone, $email, $payment, $productId, $productName, $productQuantity, $productPrice, $subtotal, $total, $orderNotes, $password, $userId, $guest, $mac, $size);
                } else {
                    $array = 'User Already exist!';
                }
            }
        } else {                           // Guest '0' and it is existing user
            $array = $this->Product_Model->PlaceOrder($firstName, $lastName, $companyName, $country, $street, $city, $state, $zip, $phone, $email, $payment, $productId, $productName, $productQuantity, $productPrice, $subtotal, $total, $orderNotes, $password, $userId, $guest, $mac, $size);
        }


        if (!empty($array)) {
            //$res = array('Response' => 'done');
            $res = $array;
        } else {
            $res = array('Response' => 'Failed');
        }

        return $res;
    }

    public function GetEventsById()
    {

        $uid = $this->input->get('uid');
        /* $email = $this->input->get('email');
          $appId = $this->input->get('appId'); */

        $array = $this->Resource_Event_Model->get_events(array('tbl_resource_events.userId' => $uid));

        /* echo "<pre>";
          print_r($array); */

        if (!empty($array)) {
            $hash = $array[0]['id'];
            if (isset($hash) > "0") {
                $i = 0;
                foreach ($array as $Arr) {
                    $res["Response"][$i] = array('id' => $Arr['id'], 'Location' => $Arr['Location'], 'EventDate' => $Arr['EventDate'], 'StartTime' => $Arr['StartTime'], 'EndTime' => $Arr['EndTime'], 'Longitude' => $Arr['Longitude'], 'Latitude' => $Arr['Latitude'], 'NoOfCars' => $Arr['NoOfCars'], 'PaymentMethod' => $Arr['PaymentMethod'], 'TotalAmount' => $Arr['TotalAmount'], 'BookingStatus' => $Arr['BookingStatus'], 'leader' => $Arr['leader']);
                    $i++;
                }
                return $res;

                //return array('id' => 'Success', 'userId' => $array[0]['userId']);
            } else {
                return array('Response' => 'No Resource');
            }
        } else {
            return array('Response' => 'Invalid User');
        }

        return $this->apiResponse;
    }

    public function get_password_recovery_secret_code()
    {
        $response = $this->User_Model->get_user(array('Email' => $this->input->get('email')));
        if (!empty($response)) {
            $userId = $response[0]['UserId'];
            $email = $this->input->get('email');
            $secretCode = vpa_random_string();
            $data = array(
                'PasswordRecoveryId' => '',
                'UserId' => $userId,
                'SecretCode' => $secretCode,
                'PasswordRecoveryStatus' => 1
            );
            $response = $this->Account_Settings_Model->insert_secret_code($data);
            return array('Response' => array('Message' => 'Success', 'UserId' => $userId, 'SecretCode' => $secretCode));
        }
        return array('Response' => array('Message' => 'Invalid user email', 'UserId' => '', 'SecretCode' => ''));
    }

    public function reset_password()
    {
        if (isset($_GET['userId'], $_GET['newPassword'], $_GET['secretCode'])) {
            $newPassword = password_hash($_GET['newPassword'], PASSWORD_DEFAULT);
            $secretCode = $_GET['secretCode'];
            $userId = $_GET['userId'];
            $data = array('UserId' => $userId, 'SecretCode' => $secretCode);
            $response = $this->Account_Settings_Model->get_secret_code($data);
            if (!empty($response)) {
                $this->Account_Settings_Model->delete_secret_code($data);
                $data['Password'] = $newPassword;
                unset($data['SecretCode']);
                $this->User_Model->update_user_password($data);
                return array('Response' => 'Password has been recovered.');
            }
            return array('Response' => 'Invalid Secret Code / User Id');
        }
        return $this->apiResponse;
    }

    public function change_password()
    {

        $response = array('Message' => 'Failed to change password.', 'Status' => false);

        $email = $this->input->get('email');
        $oldPass = $this->input->get('old');
        $newPass = $this->input->get('new');
        $table = 'tbl_user';

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $response['Message'] = "Invalid email format.";
            $response['Status'] = false;
            return $response;
        }

        $result = $this->db->get_where($table, array('email' => $email, 'pass' => $oldPass), 1, 0)->result_array();

        if (!empty($result)) {
            $this->db->set('pass', $newPass);
            $this->db->where('email', $email);
            $this->db->update($table);
            $response['Message'] = "Successfully change password.";
            $response['Status'] = true;
        }

        return $response;
    }

    public function edit_user()
    {
        $response = array('Response' => array('Data' => '', 'Message' => '', 'Status' => false));
        $data = $this->input->get();
        $table = 'tbl_user';

        if (!isset($data['id'])) {
            return $response;
        }

        if (isset($data['email']) && !filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            $response['Response']['Message'] = "Invalid email format.";
            return $response;
        }

        $id = $data['id'];
        unset($data['id'], $data['request']);

        $result = $this->db->get_where($table, array('id' => $id), 1, 0)->result_array();
        if (!empty($result)) {
            $this->db->set($data);
            $this->db->where('id', $id);
            $this->db->update($table);
            $response['Response']['Message'] = "Successfully updated.";
            $response['Response']['Status'] = true;
            $updatedResult = $this->db->get_where($table, array('id' => $id), 1, 0)->result_array();
            foreach ($updatedResult[0] as $key => $value) {
                if (is_null($value)) {
                    $updatedResult[0][$key] = "";
                }
            }
            $response['Response']['Data'] = $updatedResult;
        } else {
            $response['Response']['Message'] = "Invalid User.";
        }

        return $response;
    }

    public function get_users()
    {
        $response = $this->User_Model->get_all_users();
        if (!empty($response)) {
            return array('Response' => $response);
        }
        return $this->apiResponse;
    }

    public function get_profile()
    {
        $userId = $this->input->get('userId');
        $this->apiResponse = $this->Profile_Model->get_profile(array('UserId' => $userId));
        if (!empty($this->apiResponse)) {
            $this->apiResponse[0]['ProfileImage'] = base_url() . "uploads/" . $this->apiResponse[0]['ProfileImage'];
            return array('Response' => 'Success', 'UserDetails' => $this->apiResponse);
        }
        return array('Response' => 'Failed', 'UserDetails' => '');
    }

    public function set_profile()
    {
        unset($_GET['request']);
        $data = array();
        foreach ($_GET as $key => $value) {
            $data[ucfirst($key)] = $value;
        }
        $query = $this->Profile_Model->set_profile($data);
        if ($query) {
            return array('Response' => 'Profile has been updated');
        } else {
            return array('Response' => 'Invalid data provided');
        }
        return $this->apiResponse;
    }

    public function booking()
    {

        if (!isset($_GET['request'], $_GET['paymentMethod'])) {
            return $this->apiResponse;
        }

        unset($_GET['request']);
        $this->verify_payment_method($_GET['paymentMethod']);
        $data = array();

        foreach ($_GET as $key => $value) {
            $data[ucfirst($key)] = $value;
        }

        $data['BookingId'] = '';
        $data['EventDate'] = date('Y-m-d', strtotime($data['EventDate']));
        $data['StartTime'] = date("H:i", strtotime($data['StartTime']));
        $data['EndTime'] = date("H:i", strtotime($data['EndTime']));
        $response = $this->Booking_Model->add_new_booking($data);
        if ($response) {
            $bookingDetails = $this->Booking_Model->get_booking(array('UserId' => $data['UserId']))[0];
            $notificationData = array('UserId' => $bookingDetails['UserId'], 'BookingId' => $bookingDetails['BookingId'], 'Message' => 'Your order has been booked. Your Booking Id is ' . $bookingDetails['BookingId']);
            $response = $this->send_notification($notificationData);
            $response['Message'] = "Your order has been booked.";
        } else {
            $response = array('BookingId' => '', 'Notification' => 'Not Sent.', 'Message' => 'Booking already exists');
        }

        return array('Response' => $response);
    }

    public function get_bookings($array)
    {
        if (isset($array['request']))
            unset($array['request']);
        $data = array();
        $newResp = array();
        foreach ($array as $key => $value) {
            $key = 'tbl_bookings.' . ucfirst($key);
            $data[$key] = $value;
        }
        return $this->Booking_Model->get_history($data);
    }

    public function history()
    {
        $response = $this->get_bookings($_GET);
        if (!empty($response)) {
            foreach ($response as $key) {
                $newResp[] = array(
                    'Day' => date("l", strtotime($key['EventDate'])),
                    'Amount' => "PKR " . vpa_format_currency($key['TotalAmount'], 'PKR'),
                    'Time' => date("d M", strtotime($key['EventDate'])) . ", " . date("h:i A", strtotime($key['StartTime'])) . ' to ' . date("h:i A", strtotime($key['EndTime'])),
                    'Location' => $key['Location'],
                    'NoOfCars' => $key['NoOfCars'] . " Cars"
                );
            }

            return array('Response' => $newResp);
        }
        return $this->apiResponse;
    }

    public function edit_booking()
    {
        unset($_GET['request']);

        if (isset($_GET['paymentMethod'])) {
            $this->verify_payment_method($_GET['paymentMethod']);
        }

        $data = array();
        foreach ($_GET as $key => $value) {
            $data[ucfirst($key)] = $value;
        }
        $query = $this->Booking_Model->edit_booking($data);
        if ($query) {
            return array('Response' => 'Order has been updated');
        } else {
            return array('Response' => 'Invalid data provided');
        }
        return $this->apiResponse;
    }

    public function add_payment_method()
    {

        unset($_GET['request']);
        $data = array();
        foreach ($_GET as $key => $value) {
            $data[ucfirst($key)] = $value;
        }
        $data['PaymentMethodId'] = '';
        $query = $this->Payment_Method_Model->add_payment_method($data);
        if ($query) {
            return array('Response' => 'Payment method has been added.');
        } else {
            return array('Response' => 'Payment method already exists');
        }
        return $this->apiResponse;
    }

    public function get_payment_methods()
    {
        return $this->Payment_Method_Model->get_payment_methods();
    }

    public function verify_payment_method($paymentMethod)
    {
        $arrayPaymentMethods = $this->get_payment_methods();
        if (!vpa_multi_in_array($arrayPaymentMethods, 'PaymentMethodId', $paymentMethod)) {
            echo json_encode(array('Response' => 'Invalid Payment Method'));
            die;
        }
    }

    public function add_query()
    {
        unset($_GET['request']);
        $data = array();
        foreach ($_GET as $key => $value) {
            $data[ucfirst($key)] = $value;
        }
        $data['SupportId'] = '';
        $reponse = $this->Support_Model->add_query($data);
        if ($reponse) {
            return array('Response' => 'Query/Complaint has been reported.');
        } else {
            return array('Response' => 'Query/Complaint already exists');
        }
        return $this->apiResponse;
    }

    public function get_queries()
    {
        unset($_GET['request']);
        $data = array();
        $newResp = array();
        foreach ($_GET as $key => $value) {
            $data[ucfirst($key)] = $value;
        }
        $response = $this->Support_Model->get_queries($data);
        if (!empty($response)) {
            return array('Response' => $response);
        }
        return $this->apiResponse;
    }

    public function edit_query()
    {
        unset($_GET['request']);

        $data = array();
        foreach ($_GET as $key => $value) {
            $data[ucfirst($key)] = $value;
        }
        $response = $this->Support_Model->edit_query($data);
        if ($response) {
            return array('Response' => 'Query has been updated');
        } else {
            return array('Response' => 'Invalid data provided');
        }
        return $this->apiResponse;
    }

    public function get_app_info()
    {
        $response = $this->Info_Model->get_app_info();
        unset($response[0]['AppId']);
        return $response;
    }

    public function edit_app_info()
    {
        unset($_GET['request']);

        $data = array();
        foreach ($_GET as $key => $value) {
            $data[ucfirst($key)] = $value;
        }
        $data['AppId'] = 0;
        $response = $this->Info_Model->edit_app_info($data);
        if ($response) {
            return array('Response' => 'App Info has been updated');
        } else {
            return array('Response' => 'Invalid data provided');
        }
        return $this->apiResponse;
    }

    public function add_fcm_mac()
    {
        unset($_GET['request']);
        $data = array();
        foreach ($_GET as $key => $value) {
            $data[ucfirst($key)] = $value;
        }
        $response = $this->Account_Settings_Model->add_fcm_mac($data);
        if ($response) {
            return array('Response' => 'FCM has been added.');
        } else {
            return array('Response' => 'Invalid data provided');
        }
        return $this->apiResponse;
    }

    public function get_fcm_mac($userId)
    {
        return $this->Account_Settings_Model->get_fcm_mac(array('UserId' => $userId));
    }

    public function send_notification($notificationData)
    {

        $response = $this->Notification_Model->add_notification($notificationData);
        $newData = $this->get_fcm_mac($notificationData['UserId']);
        $notificationData['FcmToken'] = $newData['FcmToken'];

        $msg = array(
            'body' => $notificationData['Message'],
            'title' => "Confirmation",
            'icon' => 'ic_bg',
            'sound' => ''
        );

        $fields = array(
            'to' => $notificationData['FcmToken'],
            'notification' => $msg
        );

        $fcm_resp = (array) json_decode(vpa_fcm_notification($fields));
        $apiResponse = array('BookingId' => $notificationData['BookingId']);
        if ($fcm_resp['success'] == '0') {
            $apiResponse['Notification'] = 'Not sent.';
        } else {
            $this->Notification_Model->update_notification(array('BookingId' => $notificationData['BookingId'], 'NotificationStatus' => 1));
            $apiResponse['Notification'] = 'Sent.';
        }
        return $apiResponse;
    }

    public function get_notifications()
    {
        unset($_GET['request']);
        foreach ($_GET as $key => $value) {
            $data[ucfirst($key)] = $value;
        }
        $response = $this->Notification_Model->get_notifications($data, $limit = 10, $offset = 0);
        if (!empty($response)) {
            return array('Response' => $response);
        }
        return $this->apiResponse;
    }

    public function NewUserRegister()
    {
        //$uid = $pName;
        $email = $this->input->get('email');
        $pass = $this->input->get('pass');
        /* $pName = str_replace("_", " ", $this->input->get('pName'));
          $review = str_replace("_", " ", $this->input->get('review'));
          $author = str_replace("_", " ", $this->input->get('author')); */
        //$offset = $this->input->get('offset');
        //$limit = $this->input->get('limit');



        $i = 0;
        $array = $this->Product_Model->NewUserRegister($email, $pass);

        return $array;
    }

    public function ContactUs()
    {
        //$uid = $pName;
        $type = $this->input->get('type');
        $name = $this->input->get('name');
        $contactNumber = $this->input->get('contact');
        $email = $this->input->get('email');
        $organization = $this->input->get('organization');
        $message = $this->input->get('message');
        /* $pName = str_replace("_", " ", $this->input->get('pName'));
          $review = str_replace("_", " ", $this->input->get('review'));
          $author = str_replace("_", " ", $this->input->get('author')); */
        //$offset = $this->input->get('offset');
        //$limit = $this->input->get('limit');

        $i = 0;
        $array = $this->Product_Model->ContactUs($type, $name, $contactNumber, $email, $organization, $message);

        return $array;
    }

    public function ContactUsMob()
    {

        $type = $this->input->get('type');
        $name = $this->input->get('name');
        $contactNumber = $this->input->get('contact');
        $email = $this->input->get('email');
        $organization = $this->input->get('organization');
        $message = $this->input->get('message');
        if (!preg_match("/^((923))\d{2}\d{7}$|^((03))\d{9}$/", $contactNumber)) {
            return "Invalid Contact Number";
        }
        if (preg_match("/^((03))\d{9}$/", $contactNumber)) {
            $out = ltrim($contactNumber, "0");
            $contactNumber = "92" . $out;
        }


        $i = 0;
        $array = $this->Product_Model->ContactUs($type, $name, $contactNumber, $email, $organization, $message);
        #file_get_contents("http://mobismsnow.com/1.4/api/sendsms.php?session_id=abfea4b05721537b7b64061bca5fe9e1&to=" . $contactNumber . "&text=Dear%20Customer,%0ABe%20an%20agent%20of%20Change!%0AThank%20you%20for%20visiting%20Greene.pk%0ATo%20download%20our%20App/Web,%0A%0AAndroid%20Users:%20www.greene.pk/App%0AOthers:%20www.greene.pk&mask=Greene");
        file_get_contents("http://221.120.238.30/bsms_app5/sendapi-0.3.jsp?id=03351924556&message=Dear%20Customer,%0ABe%20an%20agent%20of%20Change!%0AThank%20you%20for%20visiting%20Greene.pk%0ATo%20download%20our%20App/Web,%0A%0AAndroid%20Users:%20www.greene.pk/App%0AOthers:%20www.greene.pk&shortcode=Greene&lang=english&mobilenum=" . $contactNumber . "&&password=ptml@123456&groupname=NULL");
        return $array;
    }

    public function ContactUsNew()
    {

        $type = $this->input->get('type');
        $name = $this->input->get('name');
        $contactNumber = $this->input->get('contact');
        $email = $this->input->get('email');
        $organization = $this->input->get('organization');
        $message = $this->input->get('message');

        if (!preg_match("/^((923))\d{2}\d{7}$|^((03))\d{9}$/", $contactNumber)) {
            return array('Response' => "Invalid Contact Number", 'Status' => false);
        }
        if (preg_match("/^((03))\d{9}$/", $contactNumber)) {
            $out = ltrim($contactNumber, "0");
            $contactNumber = "92" . $out;
        }

        switch ($type):
            case "android":
                $message = "Dear%20Customer,%0A%0AThank%20you%20for%20your%20interest%20in%20Starz%20Play%20by%20Cinepax%20App.%20Kindly%20download%20the%20Android%20App%20from%20the%20below%20link.%0A%0AURL:%20play.google.com";
                break;
            case "ios":
                $message = "Dear%20Customer,%0A%0AThank%20you%20for%20your%20interest%20in%20Starz%20Play%20by%20Cinepax%20App.%20Kindly%20download%20the%20iOS%20App%20from%20the%20below%20link.%0A%0AURL:%20itunes";
                break;
            default:
                return array();
                break;
        endswitch;

        $i = 0;
        $array = $this->Product_Model->ContactUs($type, $name, $contactNumber, $email, $organization, $message);
        #file_get_contents("http://mobismsnow.com/1.4/api/sendsms.php?session_id=abfea4b05721537b7b64061bca5fe9e1&to=" . $contactNumber . "&text=Dear%20Customer,%0ABe%20an%20agent%20of%20Change!%0AThank%20you%20for%20visiting%20Greene.pk%0ATo%20download%20our%20App/Web,%0A%0AAndroid%20Users:%20www.greene.pk/App%0AOthers:%20www.greene.pk&mask=Greene");
        #echo 'http://221.120.238.30/bsms_app5/sendapi-0.3.jsp?id=03351924556&message=Dear Customer,%0A%0AThank you for your interest in Starz Play by Cinepax App. Kindly download the iOS App from the below link.%0A%0AURL: itunes&shortcode=MobiTising&lang=english&mobilenum=03368317060&password=ptml@123456&groupname=NULL';
        vpa_remote_access("http://221.120.238.30/bsms_app5/sendapi-0.3.jsp?id=03351924556&message=" . $message . "&shortcode=MobiTising&lang=english&mobilenum=" . $contactNumber . "&password=ptml@123456&groupname=NULL");
        return $array;
    }

    public function GetUserDetails()
    {
        $response = array('Response' => array());
        $uid = $this->input->get('userId');
        $data = $this->Product_Model->GetUserDetails(array('tbl_orders.userId' => $uid));

        if (empty($data)) {
            $data = $this->Product_Model->loginUserDetails($uid);
        }

        if (!empty($data)) {
            foreach ($data[0] as $key => $value) {
                if (is_null($value)) {
                    $data[0][$key] = "";
                }
            }
        }

        $response['Response'] = $data;

        return $response;

        /* echo "<pre>";
          print_r($array);
          die(); */
        //        if (!empty($array)) {
        //            $hash = '1';
        //            if (isset($hash) > "0") {
        //                $i = 0;
        //                foreach ($array as $Arr) {
        //                    $res["Response"][$i] = $Arr;
        //                    $i++;
        //                }
        //                return $res;
        //                //return array('id' => 'Success', 'userId' => $array[0]['userId']);
        //            } else {
        //                return array('Response' => 'No Resource');
        //            }
        //        } else {
        //            $array = $this->Product_Model->loginUserDetails($uid);
        //            #echo "<pre>";
        //            #print_r($array);
        //            #die();
        //            $i = 0;
        //            if (!empty($array)) {
        //                $res[$i] = array(
        //                    'Response' => 'Success',
        //                    'Id' => $array[0]['id'],
        //                    'fname' => ($array[0]['firstName'] == null ) ? "" : $array[0]['firstName'],
        //                    'lname' => ($array[0]['lastName'] == null) ? "" : $array[0]['lastName'],
        //                    'email' => ($array[0]['email'] == null) ? "" : $array[0]['email'],
        //                    'city' => ($array[0]['city'] == null) ? "" : $array[0]['city'],
        //                    'country' => ($array[0]['country'] == null) ? "" : $array[0]['country'],
        //                    'phone' => ($array[0]['phone'] == null) ? "" : $array[0]['phone'],
        //                    'street' => ($array[0]['street'] == null) ? "" : $array[0]['street']
        //                );
        //            } else {
        //                $res[$i] = array('Response' => 'Failed');
        //            }
        //
        //            return $res;
        //        }
        //
        //        return $this->apiResponse;
    }

    public function newsletter()
    {
        $response = array('Message' => '', 'Status' => false);
        $email = $this->input->get('email');
        if ($email != "") {
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $array = array('Email' => $email);
                $result = $this->db->get_where('tbl_newsletter', $array, 0, 0)->result_array();
                if (empty($result)) {
                    $this->db->insert('tbl_newsletter', $array);
                    $response['Status'] = true;
                    $response['Message'] = 'Email has been added.';
                } else {
                    $response['Message'] = 'Email already added.';
                }
            } else {
                $response['Message'] = 'Please provide a valid email.';
            }
        } else {
            $response['Message'] = 'Please provide email.';
        }
        return $response;
    }

    public function forgotPassword()
    {

        $response = array('Message' => '', 'Status' => false);
        $email = $this->input->get('email');
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $result = $this->db->get_where('tbl_user', array('email' => $email))->result_array();

            if (!empty($result)) {

                $config = array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'mail.greene.pk',
                    'smtp_port' => 587,
                    'smtp_user' => 'info@greene.pk',
                    'smtp_pass' => 'greene@321$'
                );

                $this->load->library('email', $config);
                $this->email->set_newline("\r\n");
                $this->email->from('info@greene.pk', 'Greene');
                $this->email->to($email);
                $this->email->subject('Password Recovery');
                $this->email->message('Dear User, Your password is ' . $result[0]['pass']);
                $this->email->send();

                $response['Status'] = true;
                $response['Message'] = 'Password has been sent to your email.';
            } else {
                $response['Message'] = 'Please provide a valid email.';
            }
        } else {
            $response['Message'] = 'Please provide a valid email.';
        }
        return $response;
    }

    protected function greeni_auth()
    {
        $username = $this->input->get('username');
        $password = $this->input->get('password');
        $url = "http://www.greene.pk/api/user/generate_auth_cookie/?username=" . $username . "&password=" . $password . "&insecure=cool";
        $response['Response'] = json_decode(file_get_contents($url), True);
        return $response;
    }

    protected function greeni_signup()
    {
        $urlToGetNonce = "http://www.greene.pk/api/get_nonce/?controller=user&method=register";
        $nonce = json_decode(file_get_contents($urlToGetNonce), True)['nonce'];

        $username = $this->input->get('username');
        $password = $this->input->get('password');
        $email = $this->input->get('email');

        $urlToRegister = "http://www.greene.pk/api/user/register/?username=" . $username . "&email=" . $email . "&nonce=" . $nonce . "&display_name=" . $username . "&user_pass=" . $password . "&insecure=cool";
        $response['Response'] = json_decode(file_get_contents($urlToRegister), True);

        return $response;
    }

    protected function greeni_create_customer()
    {
        try {
            $data = [
                'email' => $this->input->get('email'),
                'password' => $this->input->get('password'),
                'first_name' => $this->input->get('first_name'),
                'last_name' => $this->input->get('last_name'),
                'username' => $this->input->get('username'),
                'billing' => [
                    'first_name' => $this->input->get('first_name'),
                    'last_name' => $this->input->get('last_name'),
                    'company' => $this->input->get('company'),
                    'address_1' => $this->input->get('address_1'),
                    'address_2' => $this->input->get('address_2'),
                    'city' => $this->input->get('city'),
                    'state' => $this->input->get('state'),
                    'postcode' => $this->input->get('postcode'),
                    'country' => $this->input->get('country'),
                    'email' => $this->input->get('email'),
                    'phone' => $this->input->get('phone')
                ],
                'shipping' => [
                    'first_name' => $this->input->get('first_name'),
                    'last_name' => $this->input->get('last_name'),
                    'company' => $this->input->get('company'),
                    'address_1' => $this->input->get('address_1'),
                    'address_2' => $this->input->get('address_2'),
                    'city' => $this->input->get('city'),
                    'state' => $this->input->get('state'),
                    'postcode' => $this->input->get('postcode'),
                    'country' => $this->input->get('country')
                ]
            ];

            $response['Response'] = $this->woo->post('customers', $data);
        } catch (HttpClientException $e) {
            $response['Response'] = $e->getMessage();
        }
        return $response;
    }

    protected function greeni_update_user()
    {
        try {
            $data = [
                'first_name' => $this->input->get('first_name'),
                'last_name' => $this->input->get('last_name'),
                'billing' => [
                    'first_name' => $this->input->get('first_name'),
                    'last_name' => $this->input->get('last_name'),
                    'company' => $this->input->get('company'),
                    'address_1' => $this->input->get('address_1'),
                    'address_2' => $this->input->get('address_2'),
                    'city' => $this->input->get('city'),
                    'state' => $this->input->get('state'),
                    'postcode' => $this->input->get('postcode'),
                    'country' => $this->input->get('country'),
                    'email' => $this->input->get('email'),
                    'phone' => $this->input->get('phone')
                ],
                'shipping' => [
                    'first_name' => $this->input->get('first_name'),
                    'last_name' => $this->input->get('last_name'),
                    'company' => $this->input->get('company'),
                    'address_1' => $this->input->get('address_1'),
                    'address_2' => $this->input->get('address_2'),
                    'city' => $this->input->get('city'),
                    'state' => $this->input->get('state'),
                    'postcode' => $this->input->get('postcode'),
                    'country' => $this->input->get('country')
                ]
            ];

            $customerId = $this->input->get('customer_id');
            $response['Response'] = $this->woo->put('customers/' . $customerId, $data);
            if (isset($_GET['current_password']) && isset($_GET['new_password'])) {
                $current_password = $_GET['current_password'];
                $new_password = $_GET['new_password'];
                $url = "http://www.greene.pk/api/user/change_password/?user_id=" . $customerId . "&current_password=" . $current_password . "&new_password=" . $new_password . "&insecure=cool";
                $response['Response']->changed_password_status = file_get_contents($url);
            }
        } catch (HttpClientException $e) {
            $response['Response'] = $e->getMessage();
        }
        return $response;
    }

    protected function greeni_create_order()
    {
        $response['Response'] = array('User' => '', 'Order' => '', 'Type' => 'Normal', 'Error' => 'No error');
        try {

            $customer_id = $this->input->get('customer_id');

            if ($customer_id == "" || $customer_id == 0) {
                $response['Response']['Type'] = 'Guest';
            }

            if ($this->input->get('create_user') == 'yes') {
                $createUserResponse = $this->greeni_create_customer()['Response'];
                $response['Response']['User'] = $createUserResponse;
                if (!isset($createUserResponse->id)) {
                    return $response;
                } else {
                    $customer_id = $createUserResponse->id;
                }
                $response['Response']['Type'] = 'New';
            }

            $items = explode("|", $this->input->get('line_items'));
            //print_r($items);
            $lineItems = array();
            foreach ($items as $item) {
                $value = explode(',', $item);
                if ($value[1] == 'no') {
                    $lineItems[] = [
                        'product_id' => $value[0],
                        'quantity' => $value[2]
                    ];
                } else {
                    $lineItems[] = [
                        'product_id' => $value[0],
                        'variation_id' => $value[1],
                        'quantity' => $value[2]
                    ];
                }
            }

            $data = [
                'customer_id' => $customer_id,
                'payment_method' => $this->input->get('payment_method'),
                'payment_method_title' => $this->input->get('payment_method_title'),
                //'status' => 'Processing',
                'set_paid' => false,
                'billing' => [
                    'first_name' => $this->input->get('first_name'),
                    'last_name' => $this->input->get('last_name'),
                    'address_1' => $this->input->get('address_1'),
                    'address_2' => $this->input->get('address_2'),
                    'city' => $this->input->get('city'),
                    'state' => $this->input->get('state'),
                    'postcode' => $this->input->get('postcode'),
                    'country' => $this->input->get('country'),
                    'email' => $this->input->get('email'),
                    'phone' => $this->input->get('phone')
                ],
                'shipping' => [
                    'first_name' => $this->input->get('first_name'),
                    'last_name' => $this->input->get('last_name'),
                    'address_1' => $this->input->get('address_1'),
                    'address_2' => $this->input->get('address_2'),
                    'city' => $this->input->get('city'),
                    'state' => $this->input->get('state'),
                    'postcode' => $this->input->get('postcode'),
                    'country' => $this->input->get('country')
                ],
                'shipping_lines' => [
                    [
                        'method_id' => $this->input->get('method_id'),
                        'method_title' => $this->input->get('method_title'),
                        'total' => $this->input->get('total')
                    ]
                ],
                'line_items' => $lineItems
            ];
            //print_r($data);
            $this->greeni_update_user();

            $response['Response']['Order'] = $this->woo->post('orders', $data);
        } catch (HttpClientException $e) {
            $response['Response']['Error'] = $e->getMessage();
        }
        return $response;
    }

    protected function greeni_get_orders_emails()
    {

        // $config = array(
        //     'protocol' => 'smtp',
        //     'smtp_host' => 'smtp.gmail.com',
        //     'smtp_port' => 587,
        //     'smtp_user' => 'shahzaib.mobitising@gmail.com',
        //     'smtp_pass' => 'aptech@123',
        //     'mailtype'  => 'html',
        //     'charset'   => 'iso-8859-1'
        // );

        // $this->email->initialize($config);

        // $this->email->from('shahzaib.khan@greene.pk', 'Greene Testing');
        // $this->email->to('shahzaib.khan@mobitising.com.pk');

        // $this->email->subject('Email Test');
        // $this->email->message('Testing the email class.');

        // $this->email->send();

        // echo $this->email->print_debugger();

        $newOrders = $this->woo->get('orders');
        if (!empty($newOrders)) {
            $meta['order_id'] = [];
            $meta['date_created'] = [];
            foreach ($newOrders as $key => $val) {
                array_push($meta['order_id'], $val->id);
                array_push($meta['date_created'], $val->date_created);
            }

            for ($i = 0; $i < count($meta['order_id']); $i++) {


                $newTime = strtotime('-1 minutes');
                // Current date and time
                $datetime = date("Y-m-d H:i", $newTime);

                // Convert datetime to Unix timestamp
                $timestamp = strtotime($datetime);

                // Subtract time from datetime
                $time = $timestamp - (5);

                // Date and time after subtraction
                $datetime = date("Y-m-d H:i", $time);


                $removeTDate =  str_replace('T', ' ', $meta['date_created'][$i]);
                $orderDateTime =  date('Y-m-d g:i', strtotime($removeTDate));


                if ($datetime == date($orderDateTime)) {
                    echo '<p>Email will send</p>';
                } else {
                    echo '<p>Email will not send</p>';
                }
            }

            // echo '<pre>';
            // print_r($newOrders);
            // echo '</prev>';
        }
    }

    protected function greeni_get_orders()
    {
        try {

            foreach ($_GET as $key => $value) {
                $data[$key] = $value;
            }

            $response['Response'] = $this->woo->get('orders', $data);
        } catch (HttpClientException $e) {
            $response['Response'] = $e->getMessage();
        }
        return $response;
    }

    protected function greeni_get_products()
    {
        try {

            foreach ($_GET as $key => $value) {
                $data[$key] = $value;
            }

            $products = json_decode(json_encode($this->woo->get('products', $data)), true);
            foreach ($products as $product => &$val) {
                $val['description'] = trim(preg_replace("/\r|\n/", ' ', strip_tags($val['description'])));
                $val['short_description'] = trim(preg_replace("/\r|\n/", ' ', strip_tags($val['short_description'])));
                $val['price_html'] = trim(preg_replace("/\r|\n/", ' ', strip_tags($val['price_html'])));
            }

            $response['Response'] = $products;
        } catch (HttpClientException $e) {
            $response['Response'] = $e->getMessage();
        }
        return $response;
    }
    protected function greeni_get_products2()
    {
        try {

            foreach ($_GET as $key => $value) {
                $data[$key] = $value;
            }

            $products = json_decode(json_encode($this->woo->get('products', $data)), true);
            echo "<pre>";
            print_r($products);
            echo "</pre>";
            die;
            foreach ($products as $product => &$val) {
                echo $val['id'];
                //$val['short_description'] = trim(preg_replace("/\r|\n/", ' ', strip_tags($val['short_description'])));
                //$val['price_html'] = trim(preg_replace("/\r|\n/", ' ', strip_tags($val['price_html'])));
            }

            //$response['Response'] = $products;
        } catch (HttpClientException $e) {
            //$response['Response'] = $e->getMessage();
        }
        //return $response;
    }
    protected function greeni_get_products_by_id()
    {
        $id = $this->input->get('product_id');
        //if ($id == 7404) {
        return $response = json_decode(file_get_contents("https://www.greene.pk/cron/files/$id.json"), True);
        //return $response;
        //}
        /*
        $response['Response'] = $this->helper_greeni_get_products_by_id($id);
        //echo "<pre>";
        //print_r($response);
        //die;
        //if ($response['Response']['id'] == 7404) {
        
        if (isset($response['Response']['related_ids'])) {
            foreach ($response['Response']['related_ids'] as $key) {
                $response['Response']['related_products'][] = $this->helper_greeni_get_products_by_id($key);
            }
        }

        return $response;
        */
    }

    protected function helper_greeni_get_products_by_id($id)
    {
        try {
            $product = json_decode(json_encode($this->woo->get('products/' . $id)), true);
            //echo "<pre>";
            //print_r($product);
            //echo "</pre>";
            //die;
            $response = array(
                'id' => $product['id'],
                'name' => $product['name'],
                'slug' => $product['slug'],
                'date_created' => $product['date_created'],
                'type' => $product['type'],
                'description' => trim(preg_replace("/\r|\n/", ' ', strip_tags($product['description']))),
                'short_description' => trim(preg_replace("/\r|\n/", ' ', strip_tags($product['short_description']))),
                'price' => $product['price'],
                'regular_price' => $product['regular_price'],
                'sale_price' => $product['sale_price'],
                'price_html' => trim(preg_replace("/\r|\n/", ' ', strip_tags($product['price_html']))),
                'stock_quantity' => $product['stock_quantity'],
                'in_stock' => $product['in_stock'],
                'on_sale' => $product['on_sale'],
                'categories' => $product['categories'],
                'image' => $product['images'][0]['src'],
                'variations' => array(),
                'reviews' => $this->woo->get('products/' . $product['id'] . '/reviews'),
                'related_ids' => $product['related_ids']
            );
            if ($product['type'] == 'variable') {
                $variations = json_decode(json_encode($this->woo->get('products/' . $product['id'] . '/variations')), true);
                foreach ($variations as $variation) {
                    $response['variations'][] = array(
                        'id' => $variation['id'],
                        'description' => trim(preg_replace("/\r|\n/", ' ', strip_tags($variation['description']))),
                        'price' => $variation['price'],
                        'regular_price' => $variation['regular_price'],
                        'sale_price' => $variation['sale_price'],
                        'stock_quantity' => $variation['stock_quantity'],
                        'in_stock' => $variation['in_stock'],
                        'image' => $variation['image']['src'],
                        'option' => isset($variation['attributes'][0]['option']) ? $variation['attributes'][0]['option'] : ""
                    );
                }
            }
        } catch (HttpClientException $e) {
            $response = $e->getMessage();
        }
        return $response;
    }

    protected function greeni_get_product_variations()
    {
        try {
            $product = $this->input->get('product_id');
            $response['Response'] = $this->woo->get('products/' . $product . '/variations');
        } catch (HttpClientException $e) {
            $response['Response'] = $e->getMessage();
        }
        return $response;
    }

    protected function greeni_get_payment_gateways()
    {
        try {
            //$response['Response'] = $this->woo->get('payment_gateways/cod');
            $response['Response'] = $this->woo->get('payment_gateways');
        } catch (HttpClientException $e) {
            $response['Response'] = $e->getMessage();
        }
        return $response;
    }

    protected function greeni_get_customer()
    {
        try {
            $customer = $this->input->get('customer');
            $response['Response'] = $this->woo->get('customers/' . $customer);
        } catch (HttpClientException $e) {
            $response['Response'] = $e->getMessage();
        }
        return $response;
    }

    protected function greeni_reset_password()
    {
        $user = $this->input->get('user_login');
        $url = "http://www.greene.pk/api/user/retrieve_password/?user_login=" . urlencode($user) . "&insecure=cool";
        $response['Response'] = json_decode(vpa_remote_access($url), true);
        // $response['Response'] = json_decode(file_get_contents($url), True);
        return $response;
    }

    protected function greeni_get_product_categories()
    {
        try {
            foreach ($_GET as $key => $value) {
                $data[$key] = $value;
            }
            $response['Response'] = $this->woo->get('products/categories', $data);
        } catch (HttpClientException $e) {
            $response['Response'] = $e->getMessage();
        }
        return $response;
    }

    protected function greeni_get_home_content()
    {

        $content = file_get_contents("https://www.greene.pk/cron/home_content_taha.json");
        $response['Response'] = json_decode(trim(preg_replace("/\r|\n/", ' ', strip_tags($content))), true);
        return $response;
    }

    protected function _cron_greeni_write_home_content()
    {
        try {
            $items = array();
            $data = array();
            $data['order'] = 'desc';
            $data['orderby'] = 'date';
            $data['per_page'] = 5;
            $sliders = json_decode(json_encode($this->woo->get('products', ['order' => 'desc', 'orderby' => 'date', 'per_page' => 5])), true);
            foreach ($sliders as $slide) {
                // $products = vpa_array_flatten($slide['images']);
                $products['id'] = $slide['id'];
                $products['type'] = $slide['type'];
                $products['price'] = $slide['price'];
                $products['regular_price'] = $slide['regular_price'];
                $products['sale_price'] = $slide['sale_price'];
                $products['categories'] = $slide['categories'];
                $items['Slider'][] = $products;
            }
            $items['Categories'] = json_decode(json_encode($this->woo->get('products/categories', ['parent' => 0, 'per_page' => 100, 'exclude' => '44,34,91'])), true);
            $catData = ['per_page' => 100, 'exclude' => '44,34,20,32,15,25'];
            $categories = json_decode(json_encode($this->woo->get('products/categories', $catData)), true);

            $catString = "";
            foreach ($categories as $cat) {
                if ($cat['parent'] != 0) {
                    $catString = $catString . $cat['id'] . ",";
                }
            }

            $items['SubCategories'] = json_decode(json_encode($this->woo->get('products/categories', array('include' => $catString, 'per_page' => 100))), true);
            $data['per_page'] = 3;
            foreach ($categories as $category) {
                $data['category'] = $category['id'];
                $records = json_decode(json_encode($this->woo->get('products', $data)), true);
                foreach ($records as $record) {
                    // $products = vpa_array_flatten($record['images']);
                    $products['id'] = $record['id'];
                    $products['type'] = $record['type'];
                    $products['price'] = $record['price'];
                    $products['regular_price'] = $record['regular_price'];
                    $products['sale_price'] = $record['sale_price'];
                    $products['categories'] = $record['categories'];
                    $items[$category['name']][] = $products;
                }
            }
            unset($data['category'], $data['order'], $data['orderby']);

            //$fp = fopen('/var/www/vhosts/www.greene.pk/services/assets/json_file/home_content.json', 'w');	
            //$fp = fopen('/tmp/home_content.json', 'w');
            $fp = fopen('http://125.209.101.154/greeni_cron/home_content.json', 'w');
            fwrite($fp, json_encode($items));
            fwrite($fp, "Taha");
            fclose($fp);
            echo "Taha";
            $wdata = json_encode($items);
            `sudo echo "Taha" >>/tmp/home_content`;

            $response['Response'] = $items;
        } catch (HttpClientException $e) {
            $response['Response'] = $e->getMessage();
        }
        return $response;
    }

    protected function greeni_add_product_review()
    {
        try {
            $product = $this->input->get('product_id');
            $data = [
                'review' => $this->input->get('review'),
                'name' => $this->input->get('name'),
                'email' => $this->input->get('email')
            ];

            $response['Response'] = $this->woo->post('products/' . $product . '/reviews', $data);
        } catch (HttpClientException $e) {
            $response['Response'] = $e->getMessage();
        }
        return $response;
    }
}
